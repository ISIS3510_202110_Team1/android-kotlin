package com.example.wibuapp.fragments

import android.R
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.NotificationCompat
import androidx.fragment.app.Fragment
import com.example.wibuapp.code_generic_reusable.ConnectionChecking
import com.example.wibuapp.code_generic_reusable.CustomDialog
import com.example.wibuapp.code_generic_reusable.SendSMS
import com.example.wibuapp.databinding.FragmentVehicleBinding
import com.example.wibuapp.entities.SafeContactEntity
import com.example.wibuapp.services.NotifyService
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.fragment_vehicle.*
import kotlin.random.Random
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase


class VehicleFragment: Fragment() {
        val database = FirebaseDatabase.getInstance()
        val firebase_storage = FirebaseStorage.getInstance()

        private var _binding: FragmentVehicleBinding? = null
        private val binding get() = _binding!!

        val auth = FirebaseAuth.getInstance()
        val currentUser = auth.currentUser
        var databaseReference: DatabaseReference? = null
        var databaseContacts: DatabaseReference? = null
        private lateinit var firebaseAnalytics: FirebaseAnalytics

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            _binding = FragmentVehicleBinding.inflate(inflater, container, false)
            val view = binding.root
            var plateValidator = Regex("^[A-Z]{3}\\d{3}$")

            var btnSave = binding.btnSaveVehicle

            firebaseAnalytics = Firebase.analytics

            btnSave.setOnClickListener {

                if(ConnectionChecking.isConnected(view.context)){
                val from = binding.editFrom
                val plate = binding.txtPlate
                val brand = binding.editBrand
                val to = binding.editTo
                val typeTransport = binding.editTypeTransport
                val send = binding.swVehicle

                if (!plateValidator.matches(plate.text.toString())) {
                    plate.setError("Please enter a valid plate")
                    return@setOnClickListener
                 } else if (TextUtils.isEmpty(from.text.toString())){
                    from.setError("Please enter origin place ")
                    return@setOnClickListener
                } else if (TextUtils.isEmpty(to.text.toString())){
                    from.setError("Please enter destination place ")
                    return@setOnClickListener
                } else if (TextUtils.isEmpty(brand.text.toString())) {
                    brand.setError("Please enter the vehicle's brand")
                    return@setOnClickListener
                } else if (TextUtils.isEmpty(typeTransport.text.toString())) {
                    typeTransport.setError("Please enter the type of transport (taxi, bus, uber)")
                    return@setOnClickListener
                } else {
                    if (send.isChecked) {
                        showNotificationTravel(activity?.applicationContext, plate.text.toString(), from.text.toString(), to.text.toString())
                        sendNotificationLeft(activity?.applicationContext, plate.text.toString(), from.text.toString(), to.text.toString())
                    }
                    add_travel(typeTransport.text.toString())
                    from.setText("")
                    plate.setText("")
                    brand.setText("")
                    to.setText("")
                    typeTransport.setText("")
                    send.isChecked = false
                }

            }
                else {
                activity?.supportFragmentManager?.let { it1 -> CustomDialog().show(it1, "MyCustomFragmentContact") }
            }
            }

            return view
        }


        override fun onDestroyView() {
            super.onDestroyView()
            _binding = null
        }

        public override fun onResume() {
            super.onResume()
            recordScreenView()
        }

        private fun recordScreenView() {

            val screenName = "vehicle-fragment"

            // [START set_current_screen]
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
                param(FirebaseAnalytics.Param.SCREEN_NAME, screenName)
                param(FirebaseAnalytics.Param.SCREEN_CLASS, "MainActivity")
            }
            // [END set_current_screen]
        }

    companion object{
        var database = FirebaseDatabase.getInstance()

        var auth = FirebaseAuth.getInstance()
        val currentUser = auth.currentUser
        var databaseReference: DatabaseReference? = null
        var databaseContacts: DatabaseReference? = null
        var databaseVehicle: DatabaseReference? = null

        fun sendNotificationLeft(context:Context?,plate:String,from:String, to:String){
            val message = "Someone has left: "+from +" in a vehicle with plate: "+plate +" with destination: "+to

            if (ConnectionChecking.isConnected(context!!)){
                auth = FirebaseAuth.getInstance()
                database = FirebaseDatabase.getInstance()
                databaseReference = database?.reference!!.child("profile")
                databaseContacts = database?.reference!!.child("contacts")


                val currentUser = auth.currentUser
                val userreference = databaseReference?.child((currentUser?.uid!!))
                userreference!!.get().addOnSuccessListener {
                    val userreference = databaseContacts?.child((currentUser?.uid!!))
                    var safeContacts: MutableList<SafeContactEntity> = ArrayList()
                    userreference!!.get()
                        .addOnSuccessListener {
                            for (ds in it.children) {
                                val name: String = ds.child("name").getValue(String::class.java)!!
                                val phone: String = ds.child("phone").getValue(String::class.java)!!
                                val photo: String = ds.child("photoURL").getValue(String::class.java)!!
                                val key: String = ds.key.toString()
                                safeContacts.add(SafeContactEntity(0, name, key, phone, photo))
                                databaseReference?.orderByChild("phoneNumber")?.equalTo(phone)?.get()?.addOnSuccessListener { profile ->

                                    if (profile != null && profile!!.hasChildren()) {
                                        NotifyService.establishNotification(
                                            message,
                                            profile.children.first().child("tokenMessage").value.toString()
                                        )
                                    }
                                }
                            }
                        }
                        .addOnFailureListener { exception ->
                            Log.d("ContactsAdapterError", "Error getting documents: ", exception)
                        }
                }.addOnFailureListener { exception ->
                    Log.d("ContactsAdapterError", "Error getting documents: ", exception)
                }
            }
            else{
                SendSMS.sendingSMS(context, message)
            }
        }

        fun showNotificationTravel(context: Context?, plate:String, from:String, to:String) {
            var message = "Tap the button when you arrive to your destination to notificate your safe contacts"
            val CHANNEL_ID = "VEHICLE_NOTIFICATION_CHANNEL"
            var notificationId = 42
            notificationId += Random(notificationId).nextInt(1, 30)

            val notificateIntent = Intent(context, NotifyService::class.java).apply {
                putExtra("Arrived",true)
                putExtra("plate",plate)
                putExtra("from",from)
                putExtra("to",to)
            }

            val notificatePendingIntent: PendingIntent =
                PendingIntent.getService(context, 0, notificateIntent, 0)

            val notificationBuilder = NotificationCompat.Builder(context!!.applicationContext, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_menu_send)
                .setContentTitle("Wibu")
                .setContentText(message)
                .setStyle(NotificationCompat.BigTextStyle())
                .setColor(Color.parseColor("#FF6200EE"))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .addAction(R.drawable.ic_menu_send, "Notificate",
                    notificatePendingIntent)
                .setAutoCancel(true)

            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                    CHANNEL_ID,
                    "wibu",
                    NotificationManager.IMPORTANCE_DEFAULT
                ).apply {
                    description = "wibu"
                }
                notificationManager.createNotificationChannel(channel)
            }
            notificationManager.notify(notificationId, notificationBuilder.build())
        }


        fun add_travel(typeTransport:String){
            database = FirebaseDatabase.getInstance()
            databaseVehicle = database?.reference!!.child("travels").child(typeTransport)
            databaseVehicle!!.get().addOnSuccessListener {
                if(it.value == null){
                    databaseVehicle!!.setValue(1)
                }
                else{
                    databaseVehicle!!.setValue(it.value as Long + 1)
                }
            }
        }

    }
}