package com.example.wibuapp

data class SafeContact(
    var name: String,
    var phone: String,
    var photo: String
    )