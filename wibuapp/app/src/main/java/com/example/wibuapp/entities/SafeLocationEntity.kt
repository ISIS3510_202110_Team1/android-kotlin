package com.example.wibuapp.entities

import androidx.annotation.ColorInt
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "safelocationinfo")
data class SafeLocationEntity (
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id : Int = 0,
        @ColumnInfo(name = "key") var key: String,
        @ColumnInfo(name = "user") val user: String,
        @ColumnInfo(name = "lat") val lat: Double,
        @ColumnInfo(name = "lon") val lon: Double,
        @ColumnInfo(name = "address") val address: String
)