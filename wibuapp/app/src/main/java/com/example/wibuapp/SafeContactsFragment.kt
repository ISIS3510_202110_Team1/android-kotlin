package com.example.wibuapp

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class SafeContactsFragment : Fragment() {

    lateinit var mRecyclerView: RecyclerView
    val mAdapter: SafeContactsAdapter = SafeContactsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("Entra","onCreate")
        super.onCreate(savedInstanceState)
    }

    fun getSafeContacts(): MutableList<SafeContact> {
        Log.d("Entra","getSafeContacts")
        var safeContacts: MutableList<SafeContact> = ArrayList()
        safeContacts.add(SafeContact("Tal Cual","+573138864366","https://pbs.twimg.com/profile_images/1254104062854926338/5W0Hv58g_400x400.jpg"))
        safeContacts.add(SafeContact("Tal Cual","+573138864366","https://pbs.twimg.com/profile_images/1254104062854926338/5W0Hv58g_400x400.jpg"))
        return safeContacts
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.d("Entra","onCreateViewFragment")
        val view: View =  inflater.inflate(R.layout.fragment_safe_contacts, container, false)
        mRecyclerView = view.findViewById(R.id.rvSafeContacts) as RecyclerView
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(view.context)
        mAdapter.SafeContactsAdapter(getSafeContacts())
        mRecyclerView.adapter = mAdapter
        return view
    }

}