package com.example.wibuapp.models

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.wibuapp.entities.SafeLocationEntity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.getValue

class BlogModel() {

    private val auth = FirebaseAuth.getInstance()
    private val database = FirebaseDatabase.getInstance()
    private val databaseReference = database?.reference!!.child("reminders")
    var safePlaces: MutableList<SafeLocationEntity> = ArrayList()

     fun fetchblogData(): LiveData<List<SafeLocationEntity>> {
        val mutableLiveData = MutableLiveData<List<SafeLocationEntity>>()
        val currentUser = auth.currentUser
        val currentUserDb = databaseReference?.child((currentUser?.uid!!))
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
                if(safePlaces.size != 0){
                    safePlaces = ArrayList()
                }
                for( i in dataSnapshot.children){
                    var item = i.getValue<SafeLocationEntity>()
                    if (item != null) {
                        item.key = i.key.toString()
                        safePlaces.add(item)
                    }
                }
                    mutableLiveData.value = safePlaces
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("firebase", "loadPost:onCancelled", databaseError.toException())
            }
        }
        currentUserDb?.addValueEventListener(postListener)
        mutableLiveData.value = safePlaces
        return mutableLiveData
    }
}