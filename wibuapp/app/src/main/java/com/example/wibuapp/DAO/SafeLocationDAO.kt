package com.example.wibuapp.DAO

import androidx.room.*
import com.example.wibuapp.entities.SafeLocationEntity

@Dao
interface SafeLocationDAO {

    @Query("SELECT * FROM safelocationinfo ORDER BY id DESC")
    fun getAllLocationInfo(): List<SafeLocationEntity>?

    @Insert
    suspend fun insertLocation(location: SafeLocationEntity?)

    @Delete
    suspend fun deleteLocation(location: SafeLocationEntity?)

    @Update
    suspend fun updateLocation(location: SafeLocationEntity?)

}