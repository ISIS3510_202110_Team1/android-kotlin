package com.example.wibuapp.fragments

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.wibuapp.code_generic_reusable.ConnectionChecking
import com.example.wibuapp.code_generic_reusable.CustomDialog
import com.example.wibuapp.R
import com.example.wibuapp.adapters.SafeContactsAdapter
import com.example.wibuapp.entities.SafeContactEntity
import com.example.wibuapp.viewmodel.SafeContactViewModel
import com.example.wibuapp.viewmodelfactory.SafeContactViewModelFactory
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import java.io.ByteArrayOutputStream
import java.util.*
import kotlin.collections.ArrayList
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase


/**
 * A simple [Fragment] subclass.
 * Use the [ContactsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ContactsFragment : Fragment() , SafeContactsAdapter.RowClickListener {

    val REQUEST_CODE = 120

    private lateinit var viewModel: SafeContactViewModel
    private lateinit var viewModelFactory: SafeContactViewModelFactory

    lateinit var mRecyclerView: RecyclerView
    lateinit var recyclerViewAdapter: SafeContactsAdapter

    private var imageUri: Uri? = null
    lateinit var imagePrev: ImageView
    lateinit var progressBar: ProgressBar
    lateinit var btnSaveContact: AppCompatButton

    var auth: FirebaseAuth = FirebaseAuth.getInstance()
    var database: FirebaseDatabase? = FirebaseDatabase.getInstance()
    val firebase_storage = FirebaseStorage.getInstance()
    val currentUser = auth.currentUser


    var databaseReference: DatabaseReference? = database?.reference!!.child("contacts")
    var databaseReferenceStorage = database?.reference!!.child("contactsPic")

    // [START declare_analytics]
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_contacts, container, false)
        mRecyclerView = view.findViewById(R.id.rvSafeContacts) as RecyclerView
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(view.context)

        recyclerViewAdapter = SafeContactsAdapter(this)
        mRecyclerView.adapter = recyclerViewAdapter

        val appTosent = getActivity()?.getApplication()

        firebaseAnalytics = Firebase.analytics

        viewModelFactory = SafeContactViewModelFactory(appTosent!!)

        viewModel = ViewModelProvider(this, viewModelFactory)
                .get(SafeContactViewModel::class.java)


        viewModel.getAllUsersObservers().observe( viewLifecycleOwner , Observer {
            recyclerViewAdapter.setListData(ArrayList(it))
            recyclerViewAdapter.notifyDataSetChanged()
        })


        onlyOneSafeContact(view.context)

        var view2 = LayoutInflater.from(container?.context).inflate(R.layout.popup_safecontact, container, false);
        var btnAdd = view.findViewById(R.id.fbnAddSafeContact) as FloatingActionButton

        btnAdd.setOnClickListener{
            if(ConnectionChecking.isConnected(view.context)){
                val popupWindow: PopupWindow = PopupWindow(container?.context)
                popupWindow.contentView = view2
                popupWindow.showAtLocation(view,Gravity.CENTER_VERTICAL,Gravity.CENTER_HORIZONTAL,Gravity.TOP)
                popupWindow.isOutsideTouchable = true
                popupWindow.isFocusable = true
                popupWindow.update()

                var nameInput = view2.findViewById(R.id.txtSafeContactName) as EditText
                var phoneInput = view2.findViewById(R.id.txtSafeContactPhone) as EditText

                // Select photo
                var photoInputSelect = view2.findViewById(R.id.selectButton)  as AppCompatButton
                // Upload photo

                imagePrev = view2.findViewById(R.id.imagePreview) as ImageView

                progressBar = view2.findViewById(R.id.progressBar) as ProgressBar


                val btnClose = view2.findViewById(R.id.btnCancelSafeContact) as Button
                btnSaveContact = view2.findViewById(R.id.btnSaveSafeContact) as AppCompatButton

                photoInputSelect.setOnClickListener{
                    val builder: AlertDialog.Builder? = activity?.let {
                        AlertDialog.Builder(it)
                    }
                    builder?.setTitle("Select image")
                    builder?.setMessage("Please select an option")
                    builder?.apply {
                        setNeutralButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                            dialog.dismiss()
                        })

                        setNegativeButton("Gallery",
                                DialogInterface.OnClickListener { dialog, id ->
                                    selectFromGallery()
                                    dialog.dismiss()
                                })
                    }
                    // Create the AlertDialog
                    val dialog = builder?.create()
                    dialog?.show()
                }

                btnSaveContact.setOnClickListener{
                    if(imageUri != null){
                        progressBar.setVisibility(View.VISIBLE)
                        val bitmap =
                                MediaStore.Images.Media.getBitmap(getActivity()?.getContentResolver(), imageUri)
                        val storageRef = firebase_storage.reference
                        val imageRef = storageRef.child("imagesContacts/" +   UUID.randomUUID().toString())

                        val baos = ByteArrayOutputStream()
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                        val data = baos.toByteArray()
                        var uploadTask = imageRef.putBytes(data)

                        val urlTask = uploadTask.continueWithTask { task ->
                            if (!task.isSuccessful) {
                                task.exception?.let {
                                    throw it
                                }
                            }
                            imageRef.downloadUrl
                        }.addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                val downloadUri = task.result
                                val currentUserDb = databaseReference?.child((currentUser?.uid!!))
                                val name  = nameInput.text.toString()
                                val phone = phoneInput.text.toString()
                                val imgUrl  = downloadUri.toString()
                                val key = currentUserDb?.push()?.key

                                if (key != null) {
                                    val contact = SafeContactEntity(0, name, key, phone, imgUrl)
                                    currentUserDb?.push().setValue(contact)
                                    viewModel.insertSafeContactInfo(contact)
                                }
                                progressBar.setVisibility(View.GONE)
                                nameInput.setText("")
                                phoneInput.setText("")
                                //photoInput.setText("") ACA TENGO QUE PONER EL IMAGE VIEW EN NULLO
                                popupWindow.dismiss()

                            } else {
                                // Handle failures
                            }
                        }

                    }

                }
                btnClose.setOnClickListener{
                    nameInput.setText("")
                    phoneInput.setText("")
                    //photoInput.setText("")
                    popupWindow.dismiss()
                }

            }
            else {
                getActivity()?.supportFragmentManager?.let { it1 -> CustomDialog().show(it1, "MyCustomFragmentContact") }
            }
        }

        return view

    }

    public override fun onResume() {
        super.onResume()
        recordScreenView()
    }

    private fun recordScreenView() {

        val screenName = "contact-fragment"

        // [START set_current_screen]
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
            param(FirebaseAnalytics.Param.SCREEN_NAME, screenName)
            param(FirebaseAnalytics.Param.SCREEN_CLASS, "MainActivity")
        }
        // [END set_current_screen]
    }

    private fun onlyOneSafeContact( context: Context){
        val currentUser = auth.currentUser
        val currentUserDb = databaseReference?.child((currentUser?.uid!!))
        if(ConnectionChecking.isConnected(context)){

        }
    }

    private fun selectFromGallery(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE){
            if(data == null || data.getData() == null){
                return
            }
            try{
                imageUri = data.getData()
                val bitmap =
                        MediaStore.Images.Media.getBitmap(getActivity()?.getContentResolver(), imageUri)

                imagePrev.setImageBitmap(bitmap)
                btnSaveContact.setVisibility(View.VISIBLE)
            }catch (e: Exception){

            }
        }
    }

        override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun onDeleteUserClickListener(user: SafeContactEntity, context: Context) {
        val currentUser = auth.currentUser
        val currentUserDb = databaseReference?.child((currentUser?.uid!!))
        if(ConnectionChecking.isConnected(context)){
            currentUserDb?.orderByChild("key")?.equalTo(user.key)?.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    throw error.toException();
                }
                override fun onDataChange(snapshot: DataSnapshot) {
                    for (data in snapshot.getChildren()) {
                        data.ref.removeValue()
                    }
                    viewModel.deleteSafeContactInfo(user)
                }

            })

        }else {
            getActivity()?.supportFragmentManager?.let { it1 -> CustomDialog().show(it1, "MyCustomFragmentContact") }
        }
    }

    override fun onItemClickListener(user: SafeContactEntity) {
        Log.d("TUJA", "ḱ")
    }
}