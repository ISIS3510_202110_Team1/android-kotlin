package com.example.wibuapp.fragments

import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.example.wibuapp.code_generic_reusable.ConnectionChecking
import com.example.wibuapp.activities.MapsActivity
import com.example.wibuapp.R
import com.example.wibuapp.databinding.FragmentHomeBinding
import com.example.wibuapp.entities.SafeContactEntity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import android.location.Geocoder
import kotlinx.android.synthetic.main.fragment_home.view.*
import android.os.Build
import androidx.annotation.RequiresApi
import com.example.wibuapp.code_generic_reusable.SendSMS
import com.example.wibuapp.data_classes.PanicButtonEvent
import com.example.wibuapp.data_classes.Statistics
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.ArrayList
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient


    private val binding get() = _binding!!

    lateinit var auth: FirebaseAuth
    var databaseReference: DatabaseReference? = null
    var database: FirebaseDatabase? = null
    var databaseContacts: DatabaseReference? = null
    var databasePanicButton: DatabaseReference? = null
    var databaseReferenceStatistics: DatabaseReference? = null

    private lateinit var preferences: SharedPreferences

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding.safeEmergencyNumbersButton.setOnClickListener{makeCurrentFragment(EmergencyNumbersFragment())}
        binding.safePlacesButton.setOnClickListener{makeCurrentFragment(SafePlacesFragment())}
        binding.registerPlateButton.setOnClickListener{makeCurrentFragment(VehicleFragment())}
        binding.blogButton.setOnClickListener{makeCurrentFragment(BlogContentFragment())}
        binding.homeFragment.logo_imagen.setOnClickListener{panicButton()}

        preferences = this.requireActivity().getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)

        firebaseAnalytics = Firebase.analytics

        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()
        databaseReference = database?.reference!!.child("profile")
        databaseContacts = database?.reference!!.child("contacts")
        databasePanicButton = database?.reference!!.child("panic_button")
        databaseReferenceStatistics = database?.reference!!.child("statisticsPanic")

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.requireContext())
        if(ActivityCompat.checkSelfPermission(this.requireActivity(), Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this.requireActivity(), arrayOf(Manifest.permission.SEND_SMS), 112)
        }
        val name = preferences?.getString("FIRSTNAME", "")
        binding.nameText.text = "Hello " + name.toString()

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    public override fun onResume() {
        super.onResume()
        recordScreenView()
    }

    private fun recordScreenView() {

        val screenName = "home-fragment"

        // [START set_current_screen]
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
            param(FirebaseAnalytics.Param.SCREEN_NAME, screenName)
            param(FirebaseAnalytics.Param.SCREEN_CLASS, "MainActivity")
        }
        // [END set_current_screen]
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun panicButton(){
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.putBoolean("PANIC_BUTTON",true)
        editor.apply()

        if (ConnectionChecking.isConnected(this.requireContext())) {
            Log.d("button","all rigth" + binding.nameText.text)
            if (ActivityCompat.checkSelfPermission(
                    this.requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this.requireContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location : Location? ->
                    // Got last known location. In some rare situations this can be null.

                    val currentUser = auth.currentUser
                    val userreference = databaseContacts?.child((currentUser?.uid!!))
                    var safeContacts: MutableList<SafeContactEntity> = ArrayList()
                    var address = location?.latitude?.let { getAddress(it,location?.longitude) }
                    val name = preferences?.getString("FIRSTNAME", "")
                    Log.d("button",name.toString() + " is in Danger! Location: " + address)
                    //regiter_panic_button_event("${location?.latitude},${location?.longitude}","click","${LocalDateTime.now()}")
                    userreference!!.get()
                        .addOnSuccessListener {
                            for (ds in it.children) {
                                val name: String = ds.child("name").getValue(String::class.java)!!
                                val phone: String = ds.child("phone").getValue(String::class.java)!!
                                val photo: String = ds.child("photoURL").getValue(String::class.java)!!
                                val key: String = ds.key.toString()
                                //safeContacts.add(SafeContact(key, name, number, photo))
                                safeContacts.add(SafeContactEntity(0, name, key, phone, photo))
                                databaseReference?.orderByChild("phoneNumber")?.equalTo(phone)?.get()?.addOnSuccessListener { profile ->
                                    if (profile != null && profile!!.hasChildren()) {
                                    }
                                    try {
                                        MapsActivity.establishNotification(
                                            binding.nameText.text.toString() + " is in Danger! Location: " + address,
                                            profile.children.first().child("tokenMessage").value.toString()
                                        )
                                    }
                                    catch(e:NoSuchElementException){
                                        Log.d("error","Usuario no existe")
                                    }


                                    }
                                }
                            }

                    val fname = preferences?.getString("FIRSTNAME","").toString()
                    val token = preferences?.getString("TOKEN","").toString()
                    var numberOfComments = 0.toLong()
                    var panicButtonTimes = 1.toLong()

                    databaseReferenceStatistics?.orderByChild("token")?.equalTo(token)?.get()?.addOnSuccessListener { statistics ->

                        if (statistics != null && statistics!!.hasChildren()) {
                            val stats = statistics.children.first()
                            val key = stats.key.toString()
                            panicButtonTimes = stats.child("panicButtonTimes").value as Long
                            panicButtonTimes++
                            databaseReferenceStatistics?.child(key)?.child("panicButtonTimes")?.setValue(panicButtonTimes)
                        }
                        else{
                            val statisticsToSend = Statistics(fname,token,panicButtonTimes,numberOfComments)
                            databaseReferenceStatistics?.push()?.setValue(statisticsToSend)
                        }
                    }
                }
                        .addOnFailureListener { exception ->
                            Log.d("ContactsAdapterError", "Error getting documents: ", exception)

                }

        }
        else{
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location : Location? ->
                    // Got last known location. In some rare situations this can be null.
                    val name = preferences?.getString("FIRSTNAME", "")
                    SendSMS.sendingSMS(this.requireContext(),name.toString() + " is in Danger, sending SMS Location: Lat:" + location?.latitude+  " Long: " + location?.longitude)
                    Log.d("button",name.toString() + " is in Danger, sending SMS Location: Lat:" + location?.latitude+  " Long: " + location?.longitude)

                }
        }
    }

    private fun makeCurrentFragment(fragment: Fragment){

        parentFragmentManager.beginTransaction().apply{
            replace(R.id.myNavHostFragment, fragment)
            commit()
        }

    }
    private fun getAddress(lat: Double, lon: Double) : String? {
        val geocoder =   Geocoder(this.requireContext(), Locale.getDefault())
        val address =   geocoder.getFromLocation(lat, lon, 1)
        return address[0].getAddressLine(0).toString()
    }
    fun regiter_panic_button_event(location:String,reason:String,date_time:String){
        val token = preferences?.getString("TOKEN", "")
        databasePanicButton!!.push().setValue(PanicButtonEvent(location,date_time,reason,token!!))
    }
}