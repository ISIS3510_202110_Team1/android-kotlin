package com.example.wibuapp.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.wibuapp.R
import com.example.wibuapp.databinding.ItemBlogListBinding
import com.example.wibuapp.databinding.ItemSafecontactListBinding
import com.example.wibuapp.databinding.ItemSafeplaceListBinding
import com.example.wibuapp.entities.SafeContactEntity


class SafeContactsAdapter(val listener: RowClickListener): RecyclerView.Adapter<SafeContactsAdapter.MyViewHolder>() {

    var items  = ArrayList<SafeContactEntity>()
    lateinit var view:View
    lateinit var parent:ViewGroup

    fun setListData(data: ArrayList<SafeContactEntity>) {
        this.items = data
    }

     override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SafeContactsAdapter.MyViewHolder {
        this.parent = parent
        val layoutInflater = LayoutInflater.from(parent.context)
        view = layoutInflater.inflate(R.layout.item_safecontact_list, parent, false)

        return MyViewHolder(view, listener)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: SafeContactsAdapter.MyViewHolder, position: Int) {

        var view2 = LayoutInflater.from(parent.context).inflate(
                R.layout.popup_safecontact,
                parent,
                false
        )

        holder.itemView.setOnClickListener {
            listener.onItemClickListener(items[position])
        }
        holder.bind(items[position],parent.context)
    }

    class MyViewHolder(view: View, val listener: RowClickListener): RecyclerView.ViewHolder(view) {

        val safeContactName = ItemSafecontactListBinding.bind(view).tvSafeContactName
        val safeContactPhone = ItemSafecontactListBinding.bind(view).tvSafeContactPhone
        val avatar = ItemSafecontactListBinding.bind(view).ivSafeContactAvatar
        var deleteButton = ItemSafecontactListBinding.bind(view).deleteContact



        fun bind(data: SafeContactEntity, context: Context) {

            Glide.with(context)
                    .load(data.photoURL)
                    .into(avatar);

            safeContactName.text = data.name
            safeContactPhone.text = data.phone

            deleteButton.setOnClickListener {
                Log.d("error","${data.key}")
                listener.onDeleteUserClickListener(data, it.context)
            }
        }
    }

    interface RowClickListener{
        fun onDeleteUserClickListener(user: SafeContactEntity, context: Context)
        fun onItemClickListener(user: SafeContactEntity)

    }
}
