package com.example.wibuapp.fragments

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.wibuapp.R
import com.example.wibuapp.adapters.EmergencyNumbersAdapter
import com.example.wibuapp.entities.EmergencyNumbersEntity
import com.example.wibuapp.viewmodel.EmergencyNumbersViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase


/**
 * A simple [Fragment] subclass.
 * Use the [EmergencyNumbersFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EmergencyNumbersFragment : Fragment() , EmergencyNumbersAdapter.RowClickListener {

    val PHONE_CALL_CODE = 189

    lateinit var viewModel: EmergencyNumbersViewModel
    lateinit var mRecyclerView: RecyclerView
    lateinit var recyclerViewAdapter: EmergencyNumbersAdapter

    var auth: FirebaseAuth = FirebaseAuth.getInstance()
    var database: FirebaseDatabase? = FirebaseDatabase.getInstance()
    val currentUser = auth.currentUser

    var databaseReference: DatabaseReference? = database?.reference!!.child("emergencynumbers")

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_emergency_numbers, container, false)
        mRecyclerView = view.findViewById(R.id.listViewNumber) as RecyclerView
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(view.context)

        recyclerViewAdapter = EmergencyNumbersAdapter(this)
        mRecyclerView.adapter = recyclerViewAdapter

        firebaseAnalytics = Firebase.analytics

        //viewModel = ViewModelProvider(this).get(EmergencyNumbersViewModel::class.java)
        viewModel = ViewModelProviders.of(this).get(EmergencyNumbersViewModel::class.java)

        viewModel.getAllNumbersObservers().observe( viewLifecycleOwner , Observer {
            recyclerViewAdapter.setListData(ArrayList(it))
            recyclerViewAdapter.notifyDataSetChanged()
        })

        return view

    }

    public override fun onResume() {
        super.onResume()
        recordScreenView()
    }

    private fun recordScreenView() {

        val screenName = "emergencynumber-fragment"

        // [START set_current_screen]
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
            param(FirebaseAnalytics.Param.SCREEN_NAME, screenName)
            param(FirebaseAnalytics.Param.SCREEN_CLASS, "MainActivity")
        }
        // [END set_current_screen]
    }

    override fun onItemClickListener(numbers: EmergencyNumbersEntity) {
        viewModel.increment_emergency_number_count(numbers.name)
        var phoneNumber = numbers.phone

        if(ActivityCompat.checkSelfPermission(this.requireActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this.requireActivity(), arrayOf(Manifest.permission.CALL_PHONE), PHONE_CALL_CODE)
        } else {
            val intent = Intent(Intent.ACTION_DIAL).apply {
                data = Uri.parse("tel:$phoneNumber")
            }
            if (activity?.packageManager?.let { intent.resolveActivity(it) } != null) {
                startActivity(intent)
            }
        }

    }


}