package com.example.wibuapp.fragments

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.wibuapp.code_generic_reusable.ConnectionChecking
import com.example.wibuapp.code_generic_reusable.CustomDialog
import com.example.wibuapp.activities.MapsActivity
import com.example.wibuapp.R
import com.example.wibuapp.adapters.SafePlacesAdapter
import com.example.wibuapp.databinding.FragmentBlogBinding
import com.example.wibuapp.entities.SafeLocationEntity
import com.example.wibuapp.viewmodel.SafeLocationViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SafePlacesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

class SafePlacesFragment: Fragment() , SafePlacesAdapter.RowClickListenerPlaces {

    lateinit var viewModel: SafeLocationViewModel
    lateinit var mRecyclerView: RecyclerView
    lateinit var recyclerViewAdapter: SafePlacesAdapter
    var mapOpened: Boolean = false

    var auth: FirebaseAuth = FirebaseAuth.getInstance()
    var database: FirebaseDatabase? = FirebaseDatabase.getInstance()
    var databaseReference: DatabaseReference? = database?.reference!!.child("reminders")

    private var _binding: FragmentBlogBinding? = null
    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding!!

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_blog, container, false)
        mRecyclerView = view.findViewById(R.id.recyclerViewPlaces) as RecyclerView
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(view.context)

        recyclerViewAdapter = SafePlacesAdapter(this)
        mRecyclerView.adapter = recyclerViewAdapter

        firebaseAnalytics = Firebase.analytics

        viewModel = ViewModelProviders.of(this).get(SafeLocationViewModel::class.java)

        viewModel.getAllLocationObservers().observe( viewLifecycleOwner , Observer {
            recyclerViewAdapter.setListData(ArrayList(it))
            recyclerViewAdapter.notifyDataSetChanged()
        })

        var btnAdd =  view.findViewById(R.id.fbnAddSafePlace) as FloatingActionButton

        btnAdd.setOnClickListener{
            //TODO Definir como dejarlo.
            if(true || ConnectionChecking.isConnected(view.context)){
                mapOpened = true
                val intent = Intent(this.activity, MapsActivity::class.java)
                startActivity(intent)
            }
            else {
                getActivity()?.supportFragmentManager?.let { it1 -> CustomDialog().show(it1, "MyCustomFragmentContact") }
            }
        }
        return view
    }

    fun sendZeroPlacesN(){

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onResume() {
        super.onResume()
        if(mapOpened){
            parentFragmentManager.beginTransaction().apply{
                replace(R.id.myNavHostFragment, SafePlacesFragment())
                commit()
            }
            mapOpened=false
        }
        recordScreenView()
    }

    private fun recordScreenView() {

        val screenName = "safeplaces-fragment"

        // [START set_current_screen]
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
            param(FirebaseAnalytics.Param.SCREEN_NAME, screenName)
            param(FirebaseAnalytics.Param.SCREEN_CLASS, "MainActivity")
        }
        // [END set_current_screen]
    }

    override fun onDeleteLocationClickListener(location: SafeLocationEntity, context: Context) {
        val currentUser = auth.currentUser
        val currentUserDb = databaseReference?.child((currentUser?.uid!!))

        if(ConnectionChecking.isConnected(context)){
            currentUserDb?.orderByChild("key")?.equalTo(location.key)?.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    throw error.toException();
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    for (data in snapshot.getChildren()) {
                        data.ref.removeValue()
                    }
                    viewModel.deleteSafeLocationInfo(location)
                }

            })

        }else {
            getActivity()?.supportFragmentManager?.let { it1 -> CustomDialog().show(it1, "MyCustomFragmentContact") }
        }


    }

    override fun onItemClickListener(location: SafeLocationEntity) {
    }


}
