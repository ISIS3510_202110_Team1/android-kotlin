package com.example.wibuapp.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.example.wibuapp.code_generic_reusable.ConnectionChecking
import com.example.wibuapp.code_generic_reusable.SendSMS
import com.example.wibuapp.entities.SafeContactEntity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.functions.FirebaseFunctions

class NotifyService : Service() {

    lateinit var auth: FirebaseAuth
    var databaseReference: DatabaseReference? = null
    var databaseContacts: DatabaseReference? = null

    var database: FirebaseDatabase? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent!!.getBooleanExtra("Arrived",false)){
            val plate = intent!!.getStringExtra("plate")
            val from = intent!!.getStringExtra("from")
            val to = intent!!.getStringExtra("to")
            val message = "Someone has arrived from: "+from +" to: " + to +" in a vehicle with plate: "+plate
            //TODO Centralize this code in order to reduce duplicates
            if (ConnectionChecking.isConnected(this)){
                auth = FirebaseAuth.getInstance()
                database = FirebaseDatabase.getInstance()
                databaseReference = database?.reference!!.child("profile")
                databaseContacts = database?.reference!!.child("contacts")

                val currentUser = auth.currentUser
                val userreference = databaseReference?.child((currentUser?.uid!!))
                userreference!!.get().addOnSuccessListener {
                        val userreference = databaseContacts?.child((currentUser?.uid!!))
                        var safeContacts: MutableList<SafeContactEntity> = ArrayList()
                        userreference!!.get()
                            .addOnSuccessListener {
                                for (ds in it.children) {
                                    val name: String = ds.child("name").getValue(String::class.java)!!
                                    val phone: String = ds.child("phone").getValue(String::class.java)!!
                                    val photo: String = ds.child("photoURL").getValue(String::class.java)!!
                                    val key: String = ds.key.toString()
                                    //safeContacts.add(SafeContact(key, name, number, photo))
                                    safeContacts.add(SafeContactEntity(0, name, key, phone, photo))
                                    databaseReference?.orderByChild("phoneNumber")?.equalTo(phone)?.get()?.addOnSuccessListener { profile ->

                                        if (profile != null && profile!!.hasChildren()) {
                                            var tokenMessage = profile.children.first().child("tokenMessage").value.toString()
                                            establishNotification(
                                                message,
                                                tokenMessage
                                            )
                                        }
                                    }
                                }
                            }
                            .addOnFailureListener { exception ->
                                Log.d("ContactsAdapterError", "Error getting documents: ", exception)
                            }
                }.addOnFailureListener { exception ->
                    Log.d("ContactsAdapterError", "Error getting documents: ", exception)
                }
            }
            else{
                SendSMS.sendingSMS(this, message)
            }
        }

        intent.putExtra("Arrived",false)
        intent.putExtra("plate","")
        intent.putExtra("from","")
        intent.putExtra("to","")

        this.stopSelf()
        return Service.START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }
    companion object {
        fun establishNotification(mess: String, toke: String) {
            val title = "Wibu"

            val mFunctions = FirebaseFunctions.getInstance()

            val dataListA = hashMapOf(
                "title" to title,
                "body" to mess,
                "user" to toke
            )

            mFunctions.getHttpsCallable("sendNotification")
                .call(dataListA)
                .addOnSuccessListener {
                    Log.d("VICTORIA", "Send message to client check")
                }
        }
    }
}