package com.example.wibuapp.DAO

import androidx.room.*
import com.example.wibuapp.entities.EmergencyNumbersEntity

@Dao
interface EmergencyNumbersDAO {

    @Query("SELECT * FROM emergencynumber ORDER BY id DESC")
    suspend fun getAllEmergencyNumber(): List<EmergencyNumbersEntity>?

    @Insert
    suspend fun insertNumber(numbers: EmergencyNumbersEntity?)

}
