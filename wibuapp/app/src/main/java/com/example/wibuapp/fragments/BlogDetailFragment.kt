package com.example.wibuapp.fragments

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.PopupWindow
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import com.example.wibuapp.R
import com.example.wibuapp.activities.MapsActivity
import com.example.wibuapp.data_classes.BlogContent
import com.example.wibuapp.data_classes.PlaceSuggestion
import com.example.wibuapp.data_classes.Statistics
import com.example.wibuapp.databinding.FragmentBlogDetailBinding
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.database.DatabaseReference
import com.google.firebase.ktx.Firebase

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private const val ARG_PARAM3 = "param3"
private const val ARG_PARAM4 = "param4"
private const val ARG_PARAM5 = "param5"
private const val ARG_PARAM6 = "param6"
private const val ARG_PARAM7 = "param7"
private const val ARG_PARAM8 = "param8"
private const val ARG_PARAM9 = "param9"

/**
 * A simple [Fragment] subclass.
 * Use the [BlogDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BlogDetailFragment : Fragment() {

    private var param1: String? = null
    private var param2: String? = null
    private var param3: String? = null
    private var param4: String? = null
    private var param5: String? = null
    private var param6: String? = null
    private var param7: Boolean? = null
    private var param8: Long? = null
    private var param9: Long? = null
    
    private var _binding: FragmentBlogDetailBinding? = null
    private val binding get() = _binding!!


    private val database = FirebaseDatabase.getInstance()
    private val databaseReference = database?.reference!!.child("blogentries")
    private val databaseReferenceStatistics: DatabaseReference? = database?.reference!!.child("statistics")

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
            param3 = it.getString(ARG_PARAM3)
            param4 = it.getString(ARG_PARAM4)
            param5 = it.getString(ARG_PARAM5)
            param6 = it.getString(ARG_PARAM6)
            param7 = it.getBoolean(ARG_PARAM7)
            param8 = it.getLong(ARG_PARAM8)
            param9 = it.getLong(ARG_PARAM9)
        }
    }

    public override fun onResume() {
        super.onResume()
        recordScreenView()
    }

    private fun recordScreenView() {

        val screenName = "blogdetail-fragment"

        // [START set_current_screen]
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
            param(FirebaseAnalytics.Param.SCREEN_NAME, screenName)
            param(FirebaseAnalytics.Param.SCREEN_CLASS, "MainActivity")
        }
        // [END set_current_screen]
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentBlogDetailBinding.inflate(inflater, container, false)
        val view = binding.root

        val nameAuhtor = binding.containtTitle
        val dateBlog = binding.containtDate
        val titleBlog = binding.containtTitleN
        val contentBlog = binding.containtContent
        val feelingsBlog = binding.descriptionFeelings

        firebaseAnalytics = Firebase.analytics

        nameAuhtor.text = param1
        dateBlog.text = param2
        titleBlog.text = param3
        contentBlog.text = param4
        feelingsBlog.text = param5

        binding.goBackDetail.setOnClickListener{

            parentFragmentManager.beginTransaction().apply{
                replace(R.id.myNavHostFragment, BlogContentFragment())
                addToBackStack(null)
                commit()
            }
        }

        var view2 = LayoutInflater.from(container?.context).inflate(R.layout.popup_blogcomments, container, false);

        binding.blogBtn.setOnClickListener{

            val popupWindow: PopupWindow = PopupWindow(container?.context)
            popupWindow.contentView = view2
            popupWindow.showAtLocation(view, Gravity.CENTER_VERTICAL, Gravity.CENTER_HORIZONTAL, Gravity.TOP)
            popupWindow.isOutsideTouchable = true
            popupWindow.isFocusable = true
            popupWindow.update()

            var commentEntry = view2.findViewById(R.id.txtEditComment) as EditText
            var publishBtn =  view2.findViewById(R.id.btnSaveComment)  as AppCompatButton
            var cancelBtn = view2.findViewById(R.id.btnCancelCommment)  as AppCompatButton

            publishBtn.setOnClickListener{
                if(TextUtils.isEmpty(commentEntry.text.toString())){
                    commentEntry.setError("Please enter a comment to be send")
                    return@setOnClickListener
                }else {
                    val title = "Wibu"
                    val body  = "Someone wrote a comment on your entry : ${commentEntry.text} "
                    val user = param6.toString()
                    val mFunctions = FirebaseFunctions.getInstance()

                    val dataListA = hashMapOf(
                            "title" to title,
                            "body" to body,
                            "user" to user
                    )

                    val preferences = this.activity?.getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)
                    val name = param1.toString()
                    val token = param6.toString()
                    var numberOfComments = 1.toLong()
                    var panicButtonTimes = 0.toLong()

                    databaseReferenceStatistics?.orderByChild("token")?.equalTo(token)?.get()?.addOnSuccessListener { statistics ->
                        if (statistics != null && statistics!!.hasChildren()) {
                            val stats = statistics.children.first()
                            val key = stats.key.toString()
                            numberOfComments = stats.child("numberOfComments").value as Long
                            numberOfComments++
                            databaseReferenceStatistics?.child(key)?.child("numberOfComments")?.setValue(numberOfComments)
                        }
                        else{
                            val statisticsToSend = Statistics(name,token,panicButtonTimes,numberOfComments)
                            databaseReferenceStatistics?.push()?.setValue(statisticsToSend)
                        }
                    }

                    mFunctions.getHttpsCallable("sendNotification")
                            .call(dataListA)
                            .addOnSuccessListener {
                                Log.d("VICTORIA", "Send message to client check")
                                commentEntry.setText("")
                                popupWindow.dismiss()
                            }

                }
            }

            cancelBtn.setOnClickListener{
                commentEntry.setText("")
                popupWindow.dismiss()
            }

            /*databaseReference.get().addOnSuccessListener {
                for (user in it.children) {
                    for (i in user.children) {
                        Log.d("blog", i.toString())
                        if (i.child("author").value as String == param1 && i.child("title").value as String == param3) {
                            databaseReference.child(user.key!!).child(i.key!!).setValue(BlogContent(param3!!,param1!!, param2!!,param5!!,
                                    param4!!,
                                    param8!!,
                                    param9!!+1,
                                    param6!!,
                                    param7!!))
                            break
                        }
                    }
                }
            }*/
        }

        return view
    }

    companion object {

        fun newInstance(param1: String, param2: String, param3: String, param4: String,
                        param5: String, param6: String, param7: Boolean, param8: Long,
                        param9: Long):BlogDetailFragment{
            val args: Bundle = Bundle()
            args.putSerializable(ARG_PARAM1, param1)
            args.putSerializable(ARG_PARAM2, param2)
            args.putSerializable(ARG_PARAM3, param3)
            args.putSerializable(ARG_PARAM4, param4)
            args.putSerializable(ARG_PARAM5, param5)
            args.putSerializable(ARG_PARAM6, param6)
            args.putSerializable(ARG_PARAM7, param7)
            args.putSerializable(ARG_PARAM8, param8)
            args.putSerializable(ARG_PARAM9, param9)
            val fragment = BlogDetailFragment()
            fragment.arguments = args
            return fragment
        }
        
    }


}