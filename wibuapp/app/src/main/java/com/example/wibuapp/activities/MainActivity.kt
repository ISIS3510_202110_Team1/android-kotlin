package com.example.wibuapp.activities
import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.example.wibuapp.data_classes.PanicButtonEvent
import com.example.wibuapp.code_generic_reusable.ConnectionChecking
import com.example.wibuapp.R
import com.example.wibuapp.code_generic_reusable.SendSMS
import com.example.wibuapp.data_classes.Statistics
import com.example.wibuapp.databinding.ActivityMainBinding
import com.example.wibuapp.entities.SafeContactEntity
import com.example.wibuapp.fragments.AccountFragment
import com.example.wibuapp.fragments.SafePlacesFragment
import com.example.wibuapp.fragments.ContactsFragment
import com.example.wibuapp.fragments.HomeFragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.getInstance
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.sqrt

class MainActivity : AppCompatActivity(),SensorEventListener {

        lateinit var binding: ActivityMainBinding
        private lateinit var fusedLocationClient: FusedLocationProviderClient
        lateinit var auth: FirebaseAuth
        var databaseReference: DatabaseReference? = null
        var database: FirebaseDatabase? = null
        var databaseContacts: DatabaseReference? = null
        var databasePanicButton: DatabaseReference? = null
        var databaseReferenceStatistics: DatabaseReference? = null

        private lateinit var preferences: SharedPreferences
        //respecto al uso del sensor
        private lateinit var sensorMan: SensorManager
        //Variables para calcular distancia
        private var mGravity: FloatArray? = null
        private var mAccel: Float? = null
        private var mAccelCurrent: Float? = null
        private var mAccelLast: Float? = null
        //Firebase Analytics
        //Firebase Analytics
        private var firebaseAnalytics: FirebaseAnalytics? = null



    var mapOpened: Boolean = false

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            preferences = this.getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)

            auth = FirebaseAuth.getInstance()
            database = FirebaseDatabase.getInstance()
            databaseReference = database?.reference!!.child("profile")
            databaseContacts = database?.reference!!.child("contacts")
            databasePanicButton = database?.reference!!.child("panic_button")
            databaseReferenceStatistics = database?.reference!!.child("statisticsPanic")

            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            binding = ActivityMainBinding.inflate(layoutInflater)
            setContentView(binding.root)

            binding.bottomNavigation.background = null
            binding.bottomNavigation.menu.getItem(2).isEnabled = false
            binding.fab.setOnClickListener{panicButton()}

            firebaseAnalytics = getInstance(this)

            val homeFragment = HomeFragment()
            val SafePlacesFragment = SafePlacesFragment()
            val contactsFragment = ContactsFragment()
            val accountFragment = AccountFragment()
            makeCurrentFragment(homeFragment)


            var login = intent.getBooleanExtra("Login",false)
            if (login){
                binding.bottomNavigation.selectedItemId = R.id.ic_home
            }

            binding.bottomNavigation.setOnNavigationItemSelectedListener {
                when(it.itemId){
                    R.id.ic_home -> {makeCurrentFragment(homeFragment); this.mapOpened = false}
                    R.id.ic_account -> {makeCurrentFragment(accountFragment); this.mapOpened = false}
                    R.id.ic_blog -> {makeCurrentFragment(SafePlacesFragment); this.mapOpened = true}
                    R.id.ic_contacts -> {makeCurrentFragment(contactsFragment); this.mapOpened = false}
                    else -> makeCurrentFragment(homeFragment)
                }
                true
            }
            //Inicializacion de las variables respecto al sensor
            sensorMan = getSystemService(SENSOR_SERVICE) as SensorManager
            mAccel = 0.00f
            mAccelCurrent = SensorManager.GRAVITY_EARTH
            mAccelLast = SensorManager.GRAVITY_EARTH
            sensorMan.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)?.also { accelerometer ->
            sensorMan.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL, SensorManager.SENSOR_DELAY_UI)
            }




        }

    override fun onResume() {
        super.onResume()
        if(this.mapOpened){
            makeCurrentFragment(SafePlacesFragment())
            this.mapOpened = false
        }
    }

    /**
     * Function that retrieve current location and send it to the Safe Contacts of the users.
     * @param reason indicate the reason that trigger the panic button. Default is click
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private fun panicButton(reason: String = "click"){
        if (ConnectionChecking.isConnected(this)) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location : Location? ->
                    // Got last known location. In some rare situations this can be null.
                    val currentUser = auth.currentUser
                    val userreference = databaseContacts?.child((currentUser?.uid!!))
                    var safeContacts: MutableList<SafeContactEntity> = ArrayList()
                    var address = location?.latitude?.let { getAddress(it,location?.longitude) }
                    val nameText = preferences?.getString("FIRSTNAME", "")
                    Log.d("button",nameText.toString() + " is in Danger! Location: " + address)
                    //register_panic_button_event("${location?.latitude},${location?.longitude}",reason,"${LocalDateTime.now()}")
                    userreference!!.get()
                        .addOnSuccessListener {
                            for (ds in it.children) {
                                val name: String = ds.child("name").getValue(String::class.java)!!
                                val phone: String = ds.child("phone").getValue(String::class.java)!!
                                val photo: String = ds.child("photoURL").getValue(String::class.java)!!
                                val key: String = ds.key.toString()
                                safeContacts.add(SafeContactEntity(0, name, key, phone, photo))
                                databaseReference?.orderByChild("phoneNumber")?.equalTo(phone)?.get()?.addOnSuccessListener { profile ->
                                    if (profile != null && profile!!.hasChildren()) {
                                    }
                                    try {
                                        MapsActivity.establishNotification(
                                            nameText.toString() + " is in Danger! Location: " + address,
                                            profile.children.first().child("tokenMessage").value.toString()
                                        )
                                    }
                                    catch(e:NoSuchElementException){
                                        Log.d("error","Usuario no existe")
                                    }

                                }
                            }
                        }

                    val name = preferences?.getString("FIRSTNAME","").toString()
                    val token = preferences?.getString("TOKEN","").toString()
                    var numberOfComments = 0.toLong()
                    var panicButtonTimes = 1.toLong()

                    databaseReferenceStatistics?.orderByChild("token")?.equalTo(token)?.get()?.addOnSuccessListener { statistics ->

                        if (statistics != null && statistics!!.hasChildren()) {
                            val stats = statistics.children.first()
                            val key = stats.key.toString()
                            panicButtonTimes = stats.child("panicButtonTimes").value as Long
                            panicButtonTimes++
                            databaseReferenceStatistics?.child(key)?.child("panicButtonTimes")?.setValue(panicButtonTimes)
                        }
                        else{
                            val statisticsToSend = Statistics(name,token,panicButtonTimes,numberOfComments)
                            databaseReferenceStatistics?.push()?.setValue(statisticsToSend)
                        }
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d("ContactsAdapterError", "Error getting documents: ", exception)

                }

        }
        else{
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location : Location? ->
                    // Got last known location. In some rare situations this can be null.
                    val name = preferences?.getString("FIRSTNAME", "")
                    SendSMS.sendingSMS(this, name.toString() + " is in Danger, sending SMS Location: Lat:" + location?.latitude + " Long: " + location?.longitude )

                    Log.d("button",name.toString() + " is in Danger, sending SMS Location: Lat:" + location?.latitude+  " Long: " + location?.longitude)

                }
        }
        
    }

    /**
     * Function to get an Andress given latitude and longitude
     * @param latitude
     * @param longitude
     */
    private fun getAddress(lat: Double, lon: Double) : String? {
        val geocoder =   Geocoder(this, Locale.getDefault())
        val address =   geocoder.getFromLocation(lat, lon, 1)
        return address[0].getAddressLine(0).toString()
    }

    /**
     * Function to calculate movement on the device in order to trigger the panic button.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onSensorChanged(event:SensorEvent){
        if(event.sensor.type == Sensor.TYPE_ACCELEROMETER){
            mGravity = event.values.clone()
            // Shake detection

            // Shake detection
            val x = mGravity!![0]
            val y = mGravity!![1]
            val z = mGravity!![2]
            mAccelLast = mAccelCurrent
            mAccelCurrent = sqrt((x * x + y * y + z * z))
            var delta = mAccelCurrent!! - mAccelLast!!
            mAccel = mAccel!! * 0.9f + delta
            if(mAccel!! > 8){
                panicButton("movement")
                val editor: SharedPreferences.Editor = preferences.edit()
                editor.putBoolean("PANIC_BUTTON",true)
                editor.apply()
            }
        }

    }

    /**
     * Function to register a panic button event
     */
    fun register_panic_button_event(location:String,reason:String,date_time:String){
        val token = preferences?.getString("TOKEN", "")
        databasePanicButton!!.push().setValue(PanicButtonEvent(location,date_time,reason,token!!))
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }

    /**
     * Function that changes the current fragment.
     */
    private fun makeCurrentFragment(fragment: Fragment){
        supportFragmentManager.beginTransaction().apply{
            replace(R.id.myNavHostFragment, fragment)
            commit()
        }
    }
}



