package com.example.wibuapp.models

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.wibuapp.entities.EmergencyNumbersEntity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class EmergencyNumberModel {

    private val auth = FirebaseAuth.getInstance()
    private val database = FirebaseDatabase.getInstance()
    private val databaseReference = database?.reference!!.child("emergencynumbers")
    private val databaseReferenceCount = database?.reference!!.child("emergency_numbers_count")

    var safeNumbers: MutableList<EmergencyNumbersEntity> = ArrayList()

    fun fetchNumbersData(): LiveData<List<EmergencyNumbersEntity>> {

        val mutableLiveData = MutableLiveData<List<EmergencyNumbersEntity>>()

        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
                if (safeNumbers.size != 0) {
                    safeNumbers = ArrayList()
                }
                for (ds in dataSnapshot.children) {
                    val name: String = ds.key as String
                    val phoneRetrieve: Long = ds.getValue() as Long
                    val phone = phoneRetrieve.toString()
                    safeNumbers.add(EmergencyNumbersEntity(0, name, phone))
                }
                mutableLiveData.value = safeNumbers
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.w("firebase", "loadPost:onCancelled", databaseError.toException())
            }
        }
        databaseReference?.addValueEventListener(postListener)
        mutableLiveData.value = safeNumbers
        return mutableLiveData
    }
    fun increment_count(name:String){
        databaseReferenceCount.child(name).get().addOnSuccessListener {
            if(it.value==null){
                databaseReferenceCount.child(name).setValue(1)
            }
            else{
                databaseReferenceCount.child(name).setValue(it.value as Long +1)
            }

        }

    }

}