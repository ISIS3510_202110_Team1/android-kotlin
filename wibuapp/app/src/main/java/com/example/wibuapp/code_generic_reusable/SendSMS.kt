package com.example.wibuapp.code_generic_reusable

import android.content.Context
import android.telephony.SmsManager
import com.example.wibuapp.db.RoomAppDb
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

object SendSMS {
    fun sendingSMS(context:Context?, message: String){
        val safeContactDao = context?.let { RoomAppDb.getAppDatabase(it)?.safeContactDAO() }
        CoroutineScope(Dispatchers.Default).launch {
            val list = safeContactDao?.getAllContactInfo()
            if (list != null) {
                for (contact in list){
                    var sms = SmsManager.getDefault()
                    sms.sendTextMessage(contact.phone,null, message, null, null)
                }
            }
        }

    }
}