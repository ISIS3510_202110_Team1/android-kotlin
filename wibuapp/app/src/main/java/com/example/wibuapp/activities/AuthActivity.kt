package com.example.wibuapp.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.wibuapp.*
import com.example.wibuapp.code_generic_reusable.ConnectionChecking
import com.example.wibuapp.code_generic_reusable.CustomDialog
import com.example.wibuapp.databinding.ActivityAuthBinding
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class AuthActivity : AppCompatActivity() {

    lateinit var auth: FirebaseAuth
    lateinit var binding: ActivityAuthBinding

    lateinit var sharedPreferences: SharedPreferences

    var databaseReference: DatabaseReference? = null
    var database: FirebaseDatabase? = null


    /*override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser

        if(currentUser!=null){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }*/

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_Wibuapp)

        binding = ActivityAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.registerTextView.setOnClickListener{
            val intent = Intent(this, RergActivity::class.java)
            startActivity(intent)
        }

        auth = FirebaseAuth.getInstance()

        sharedPreferences = getSharedPreferences( "SHARED_PREF", Context.MODE_PRIVATE)
        loginEmailPassword()


        binding.fabGoogle.setOnClickListener{
            Toast.makeText(this@AuthActivity, "We are working to provide you soon this option", Toast.LENGTH_LONG).show()
        }


        binding.fabFacebook.setOnClickListener{
            Toast.makeText(this@AuthActivity, "We are working to provide you soon this option", Toast.LENGTH_LONG).show()
        }

        binding.fabTwitter.setOnClickListener{
            Toast.makeText(this@AuthActivity, "We are working to provide you soon this option", Toast.LENGTH_LONG).show()
        }

    }

    /**
     * Function to log and user in the application
     */
    private fun loginEmailPassword() {

        val email = binding.emailLogin
        val password = binding.passwordLogin

        binding.loginbtn.setOnClickListener{

            if(ConnectionChecking.isConnected(this)) {
                if(TextUtils.isEmpty(email.text.toString())){
                    email.setError("Please enter email ")
                    return@setOnClickListener
                } else if(TextUtils.isEmpty(password.text.toString())  ){
                    password.setError("Please enter a valid password")
                    return@setOnClickListener
                }

                auth.signInWithEmailAndPassword(email.text.toString(), password.text.toString()).addOnCompleteListener{
                    if(it.isSuccessful){
                        MainScope().launch{
                            getTokenId()
                        }

                        val intent = Intent(this, MainActivity::class.java)
                        intent.putExtra("Login",true)
                        startActivity(intent)
                        finish()
                    }else {
                        Toast.makeText(this@AuthActivity, "Login failed, please try again", Toast.LENGTH_LONG).show()
                    }
                }
            } else {
                CustomDialog().show(supportFragmentManager, "MyCustomFragment")
            }
        }
    }

    private fun getTokenId() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->

            if (!task.isSuccessful) {

                return@OnCompleteListener
            }
            // Get new FCM registration token
            val token = task.result
            loadProfile(token.toString())
        })

    }

    private fun loadProfile(token: String) {
        database = FirebaseDatabase.getInstance()
        databaseReference = database?.reference!!.child("profile")
        val currentUser = auth.currentUser
        val userreference = databaseReference?.child((currentUser?.uid!!))
        userreference?.child("tokenMessage")?.setValue(token)

        userreference?.get()?.addOnSuccessListener {
            val name = it.child("firstname").getValue()
            val lastname = it.child("lastname").getValue()
            val editor: SharedPreferences.Editor = sharedPreferences.edit()
            editor.putString("FIRSTNAME", name.toString())
            editor.putString("LASTNAME", lastname.toString())
            editor.putString("TOKEN", token)
            editor.apply()

        }?.addOnFailureListener{
            Log.e("Error", "Error getting data", it)
        }


    }



}


