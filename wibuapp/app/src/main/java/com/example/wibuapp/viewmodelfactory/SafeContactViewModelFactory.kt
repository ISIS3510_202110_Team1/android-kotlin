package com.example.wibuapp.viewmodelfactory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.wibuapp.viewmodel.SafeContactViewModel

class SafeContactViewModelFactory (private val app: Application) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SafeContactViewModel::class.java)) {
            return SafeContactViewModel(app) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}