package com.example.wibuapp.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.wibuapp.R
import com.example.wibuapp.entities.EmergencyNumbersEntity

class EmergencyNumbersAdapter(val listener: EmergencyNumbersAdapter.RowClickListener): RecyclerView.Adapter<EmergencyNumbersAdapter.MyViewHolder>() {


    var items  = ArrayList<EmergencyNumbersEntity>()
    lateinit var view: View
    lateinit var parent: ViewGroup

    fun setListData(data: ArrayList<EmergencyNumbersEntity>) {
        this.items = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmergencyNumbersAdapter.MyViewHolder {
        this.parent = parent
        val layoutInflater = LayoutInflater.from(parent.context)
        view = layoutInflater.inflate(R.layout.emergency_number_list_item, parent, false)

        return EmergencyNumbersAdapter.MyViewHolder(view, listener)
    }

    override fun getItemCount(): Int {
        return items.size
    }


    override fun onBindViewHolder(holder: EmergencyNumbersAdapter.MyViewHolder, position: Int) {

        holder.itemView.setOnClickListener {
            listener.onItemClickListener(items[position])
        }
        holder.bind(items[position],parent.context)
    }

    class MyViewHolder(view: View, val listener: EmergencyNumbersAdapter.RowClickListener): RecyclerView.ViewHolder(view) {

        val safeNumbersName = view.findViewById(R.id.tvSafeNumberName) as TextView
        val safeNumbersPhone = view.findViewById(R.id.tvSafeNumberPhone) as TextView

        fun bind(data: EmergencyNumbersEntity, context: Context) {
            safeNumbersName.text = data.name
            safeNumbersPhone.text = data.phone
        }
    }

    interface RowClickListener{
        fun onItemClickListener(numbers: EmergencyNumbersEntity)
    }
}


