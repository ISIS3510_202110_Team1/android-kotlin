package com.example.wibuapp.auxclasses

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.wibuapp.activities.MapsActivity
import com.example.wibuapp.code_generic_reusable.ConnectionChecking
import com.example.wibuapp.entities.SafeContactEntity
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingEvent
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class GeofenceReceiver : BroadcastReceiver() {
    lateinit var text: String
    lateinit var auth: FirebaseAuth
    var databaseReference: DatabaseReference? = null
    var databaseContacts: DatabaseReference? = null

    var database: FirebaseDatabase? = null

    override fun onReceive(context: Context?, intent: Intent?) {

        if (context != null) {

            val geofencingEvent = GeofencingEvent.fromIntent(intent)
            val geofencingTransition = geofencingEvent.geofenceTransition
            if (ConnectionChecking.isConnected(context)) {
                auth = FirebaseAuth.getInstance()
                database = FirebaseDatabase.getInstance()
                databaseReference = database?.reference!!.child("profile")
                databaseContacts = database?.reference!!.child("contacts")

                val currentUser = auth.currentUser
                val userreference = databaseReference?.child((currentUser?.uid!!))
                userreference!!.get().addOnSuccessListener {
                    var userName: String = "Someone "//it.child("firstname").value.toString()
                    if (geofencingTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                            geofencingTransition == Geofence.GEOFENCE_TRANSITION_EXIT
                    ) {
                        val userreference = databaseContacts?.child((currentUser?.uid!!))
                        var safeContacts: MutableList<SafeContactEntity> = ArrayList()

                        userreference!!.get()
                                .addOnSuccessListener {
                                    for (ds in it.children) {
                                        val name: String = ds.child("name").getValue(String::class.java)!!
                                        val phone: String = ds.child("phone").getValue(String::class.java)!!
                                        val photo: String = ds.child("photoURL").getValue(String::class.java)!!
                                        val key: String = ds.key.toString()
                                        //safeContacts.add(SafeContact(key, name, number, photo))
                                        safeContacts.add(SafeContactEntity(0, name, key, phone, photo))
                                        databaseReference?.orderByChild("phoneNumber")?.equalTo(phone)?.get()?.addOnSuccessListener { profile ->

                                            if (profile != null && profile!!.hasChildren()) {

                                                text = if (geofencingTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
                                                    "Someone has enter a safe place"

                                                } else {
                                                    "Someone has left a safe place"
                                                }

                                                Log.e("MESSAGE", text)

                                                var tokenMessage = profile.children.first().child("tokenMessage").value.toString()
                                                MapsActivity.establishNotification(
                                                        text,
                                                        tokenMessage
                                                )
                                            }
                                        }

                                    }
                                }
                                .addOnFailureListener { exception ->
                                    Log.d("ContactsAdapterError", "Error getting documents: ", exception)
                                }
                    }
                }.addOnFailureListener { exception ->
                    Log.d("ContactsAdapterError", "Error getting documents: ", exception)
                }
            }
            else{
                text = if (geofencingTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
                    "Someone has enter a safe place"

                } else {
                    "Someone has left a safe place"
                }
                MapsActivity.showNoConnectionError(context, text)
            }
        }
    }


}