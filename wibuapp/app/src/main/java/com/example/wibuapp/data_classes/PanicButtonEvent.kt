package com.example.wibuapp.data_classes

data class PanicButtonEvent(
    var location: String,
    var datetime: String,
    var reason: String,
    var token: String
)
