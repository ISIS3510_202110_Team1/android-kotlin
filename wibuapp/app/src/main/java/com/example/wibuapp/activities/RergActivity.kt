package com.example.wibuapp.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.example.wibuapp.code_generic_reusable.ConnectionChecking
import com.example.wibuapp.code_generic_reusable.CustomDialog
import com.example.wibuapp.R
import com.example.wibuapp.auxclasses.DatePicker
import com.example.wibuapp.databinding.ActivityRergBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class RergActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener  {

    lateinit var auth: FirebaseAuth
    lateinit var binding: ActivityRergBinding
    var databaseReference: DatabaseReference? = null
    var database: FirebaseDatabase? = null
    lateinit var tokenToSent: String
    lateinit var gender: String
    lateinit var dateBirth: String
    lateinit var sharedPreferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityRergBinding.inflate(layoutInflater)
        setContentView(binding.root)

        sharedPreferences = getSharedPreferences( "SHARED_PREF", Context.MODE_PRIVATE)

        binding.arrowImg.setOnClickListener{
            val intent = Intent(this, AuthActivity::class.java)
            startActivity(intent)
        }

        binding.etDate.setOnClickListener{
            showDatePickerDialog()
        }

        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()
        databaseReference = database?.reference!!.child("profile")



        val spinner: Spinner = binding.spinnerGender

        // Create an ArrayAdapter using the string array and a default spinner layout
                ArrayAdapter.createFromResource(
                        this,
                        R.array.gender_array,
                        android.R.layout.simple_spinner_item
                ).also { adapter ->
                    // Specify the layout to use when the list of choices appears
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    // Apply the adapter to the spinner
                    spinner.adapter = adapter
                }


        spinner.onItemSelectedListener = this

        register()

    }

    private fun showDatePickerDialog() {
        val datePicker = DatePicker { day, month, year -> onDateSelected(day, month, year) }
        datePicker.show(supportFragmentManager, "datePicker")
    }

    private fun onDateSelected(day: Int, month: Int, year: Int) {
        binding.etDate.setText("$day / $month / $year")
        dateBirth = "$day/$month/$year"
    }

    private fun register() {


        binding.signupbtn.setOnClickListener{
            if(ConnectionChecking.isConnected(this)){

                var passwordValidator = Regex("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{4,}\$")
                var emailValidator = Regex("^([\\w\\.\\-]+)@([\\w\\-]+)((\\.(\\w){2,3})+)\$")
                var phoneValidator = Regex("^3[\\d]{9}\$")

                val firstName = binding.firstNameRegister
                val lastName = binding.lastNameRegister
                val email = binding.emailRegister
                val password = binding.passwordRegister
                val phoneNumber = binding.numbermobile


                if(TextUtils.isEmpty(firstName.text.toString())){
                    firstName.setError("Please enter first name ")
                    return@setOnClickListener
                } else if(TextUtils.isEmpty(lastName.text.toString())){
                    lastName.setError("Please enter last name")
                    return@setOnClickListener
                } else if(TextUtils.isEmpty(email.text.toString())){
                    lastName.setError("Please enter email")
                    return@setOnClickListener
                } else if(TextUtils.isEmpty(password.text.toString())){
                    lastName.setError("Please enter password")
                    return@setOnClickListener
                }
                else if(!emailValidator.matches(email.text.toString())){
                    email.setError("Please enter a valid email")
                    return@setOnClickListener
                } else if(!passwordValidator.matches(password.text.toString())){
                    password.setError("Please enter a valid password")
                    Toast.makeText(this@RergActivity, "Password must have uppercase, " +
                            "lowercase characters and numbers", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                } else if(!phoneValidator.matches(phoneNumber.text.toString())){
                    phoneNumber.setError("Please enter a valid phone number")
                    return@setOnClickListener
                }
                else {

                    auth.createUserWithEmailAndPassword(email.text.toString(), password.text.toString()).addOnCompleteListener {
                        if (it.isSuccessful) {
                            val currentUser = auth.currentUser
                            val currentUserDb = databaseReference?.child((currentUser?.uid!!))

                            currentUserDb?.child("firstname")?.setValue(firstName.text.toString())
                            currentUserDb?.child("lastname")?.setValue(lastName.text.toString())
                            currentUserDb?.child("phoneNumber")?.setValue(phoneNumber.text.toString())
                            currentUserDb?.child("tokenMessage")?.setValue("0")
                            currentUserDb?.child("gender")?.setValue(gender)
                            currentUserDb?.child("birthDate")?.setValue(dateBirth)

                            val editor: SharedPreferences.Editor = sharedPreferences.edit()
                            editor.putString("FIRSTNAME", firstName.text.toString())
                            editor.putString("LASTNAME", lastName.text.toString())
                            editor.apply()

                            Toast.makeText(this@RergActivity, "Registration Succes", Toast.LENGTH_LONG).show()
                            val intent = Intent(this, AuthActivity::class.java)
                            startActivity(intent)
                            finish()


                        } else {
                            Toast.makeText(this@RergActivity, "Registration failed, please try again", Toast.LENGTH_LONG).show()
                        }
                    }

                }
            }
            else {
                CustomDialog().show(supportFragmentManager, "MycustomeInreg")
            }
        }

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        Log.d("TUJA", position.toString())
        if(position == 0){
            gender = "He"
        } else if( position == 1){
            gender = "Her"
        } else {
            gender = "Them"
        }
    }

}