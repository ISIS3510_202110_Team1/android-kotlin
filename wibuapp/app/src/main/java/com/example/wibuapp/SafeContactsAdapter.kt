package com.example.wibuapp

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class SafeContactsAdapter: RecyclerView.Adapter<SafeContactsAdapter.ViewHolder>() {
    var safeContacts: MutableList<SafeContact> = ArrayList()

    fun SafeContactsAdapter(safeContacts: MutableList<SafeContact>){
        Log.d("Entra","safeContactsAdapter")
        this.safeContacts = safeContacts
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        Log.d("Entra","onCreateViewHolder")
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_safecontact_list, parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = safeContacts[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return safeContacts.size
    }

    class ViewHolder(view:View): RecyclerView.ViewHolder(view){
        val safeContactName = view.findViewById(R.id.tvSafeContactName) as TextView
        val safeContactPhone = view.findViewById(R.id.tvSafeContactPhone) as TextView
        val avatar = view.findViewById(R.id.ivSafeContactAvatar) as ImageView

        fun bind(safeContact: SafeContact){
            safeContactName.text = safeContact.name
            safeContactPhone.text = safeContact.phone
            avatar.loadUrl(safeContact.photo)
        }

        fun ImageView.loadUrl(url:String){
            Picasso.with(context).load(url).into(this)
        }

    }

}