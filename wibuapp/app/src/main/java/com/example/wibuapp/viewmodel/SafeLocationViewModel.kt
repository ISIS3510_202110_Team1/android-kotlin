package com.example.wibuapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.wibuapp.DAO.EmergencyNumbersDAO
import com.example.wibuapp.DAO.SafeLocationDAO
import com.example.wibuapp.models.BlogModel
import com.example.wibuapp.db.RoomAppDb
import com.example.wibuapp.entities.SafeLocationEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference

class SafeLocationViewModel(app: Application): AndroidViewModel(app)  {

    lateinit var allLocations : MutableLiveData<List<SafeLocationEntity>>
    var refSafeLocationDao: WeakReference<SafeLocationDAO?>? = null

    init{
        allLocations = MutableLiveData()
        getAllLocation()

    }

    val blogModel = BlogModel()


     fun getAllLocationObservers(): MutableLiveData<List<SafeLocationEntity>> {

        if(allLocations.value?.size != 0){
            return allLocations
        }else {
            val mutableData =  MutableLiveData<List<SafeLocationEntity>>()
            blogModel.fetchblogData().observeForever {
                mutableData.value = it
            }
            for( entity in mutableData.value!!){
                insertSafeLocationInfo(entity)
            }
            return mutableData
        }
    }

     fun getAllLocation() {
        val safeLocationDao = RoomAppDb.getAppDatabase((getApplication()))?.safeLocationDAO()
         val list = safeLocationDao?.getAllLocationInfo()
         allLocations.postValue(list)


    }

    fun updateWeakReferences(){
        if (refSafeLocationDao == null){
            val safeLocationDao = RoomAppDb.getAppDatabase(getApplication())?.safeLocationDAO()
            refSafeLocationDao = WeakReference(safeLocationDao)
        }
    }

     fun insertSafeLocationInfo(entity: SafeLocationEntity){
         updateWeakReferences()
        viewModelScope.launch(Dispatchers.IO) {
            refSafeLocationDao!!.get()?.insertLocation(entity)
        }
        getAllLocation()
    }

     fun updateSafeLocationInfo(entity: SafeLocationEntity){
         updateWeakReferences()
        viewModelScope.launch(Dispatchers.IO) {
            refSafeLocationDao!!.get()?.updateLocation(entity)
        }
        getAllLocation()
    }

    fun deleteSafeLocationInfo(entity: SafeLocationEntity){
        updateWeakReferences()
        viewModelScope.launch(Dispatchers.IO) {
            refSafeLocationDao!!.get()?.deleteLocation(entity)
        }
        getAllLocation()
    }
}