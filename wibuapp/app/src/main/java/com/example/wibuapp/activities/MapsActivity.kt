package com.example.wibuapp.activities

import android.Manifest
import android.app.AlertDialog
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.example.wibuapp.code_generic_reusable.ConnectionChecking
import com.example.wibuapp.auxclasses.GeofenceReceiver
import com.example.wibuapp.R
import com.example.wibuapp.code_generic_reusable.SendSMS
import com.example.wibuapp.data_classes.PlaceSuggestion
import com.example.wibuapp.databinding.ActivityMapsBinding
import com.example.wibuapp.db.RoomAppDb
import com.example.wibuapp.entities.SafeLocationEntity
import com.example.wibuapp.viewmodel.SafeLocationViewModel
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.messaging.FirebaseMessaging
import java.util.*
import kotlin.random.Random
import com.google.maps.android.SphericalUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.launch


const val GEOFENCE_RADIUS = 100
const val GEOFENCE_ID = "REMINDER_GEOFENCE_ID"
const val GEOFENCE_EXPIRATION = 10 * 24 * 60 * 60 * 1000 // 10 days
const val GEOFENCE_LOCATION_REQUEST_CODE = 12345
const val CAMERA_ZOOM_LEVEL = 13f
const val LOCATION_REQUEST_CODE = 123

private val TAG: String = MapsActivity::class.java.simpleName

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var geofencingClient: GeofencingClient? = null
    private lateinit var locationRequest: LocationRequest

    var inSafePlace:Boolean = false
    lateinit var binding: ActivityMapsBinding
    var stickyLat: Double? = null
    var stickyLon: Double? = null
    var currentMarker: Marker? = null

    lateinit var token: String
    lateinit var viewModel: SafeLocationViewModel

    //Database Connection

    val auth = FirebaseAuth.getInstance()
    val database = FirebaseDatabase.getInstance()
    val databaseReference = database.reference.child("reminders")

    val currentUser = auth.currentUser
    val currentUserDb = databaseReference.child((currentUser?.uid!!))

    /**
     * Method for verfying that the place is visited 3 times or more
     */
     suspend fun verifyTimesVisited(safePlace:String){
        val databasePlacesReference = database.reference.child("places")
        databasePlacesReference?.orderByChild("user")?.equalTo(currentUser?.uid!!)?.get()?.addOnSuccessListener { places->
            if (places !=null && places!!.hasChildren()) {
                for (place in places.children) {
                    var placeName = place.child("name").value.toString()
                    if (placeName.equals(safePlace)){
                        var placeTimesVisited = place.child("timesVisited").value as Long
                        var placeAcepted = place.child("accepted").value as Boolean

                        placeTimesVisited++

                        if( placeTimesVisited >= 2 && !placeAcepted){

                            //TODO: MOSTRAR NOTIFICACIÓN CON OPCIONES, SI ACEPTA placeAcepted SE VUELVE TRUE Y SE CREA EL SAFEPLACE
                            showNotification(this, "Has visitado este sitio muchas veces, quieres que sea un sitio seguro ?")

                        }
                        databasePlacesReference.child(place.key!!).child("timesVisited").setValue(placeTimesVisited)
                        break
                    }
                    else{
                        var placeSuggestion = PlaceSuggestion(safePlace, 1, false, currentUser?.uid!!)
                        databasePlacesReference?.push().setValue(placeSuggestion)
                    }

                }
            }

            else{
                var placeSuggestion = PlaceSuggestion(safePlace, 1, false, currentUser?.uid!!)
                databasePlacesReference?.push().setValue(placeSuggestion)
            }
        }.addOnFailureListener {
            Log.e("addSuggestion", it.toString())
        }


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProviders.of(this).get(SafeLocationViewModel::class.java)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        if (ConnectionChecking.isConnected(this)) {
            geofencingClient = LocationServices.getGeofencingClient(this)
            FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                token = task.result
            })
        }


        val safeLocationDao = RoomAppDb.getAppDatabase((getApplication()))?.safeLocationDAO()
        val listLocation = safeLocationDao?.getAllLocationInfo()


        if (ConnectionChecking.isConnected(this)) {
            if( listLocation != null) {
                for(item in listLocation) {
                    val latlongSent = LatLng(item.lat, item.lon)
                    createGeoFence(latlongSent,currentUser.uid, geofencingClient!!)
                }
            }
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        geofencingClient = LocationServices.getGeofencingClient(this)
        locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.setSmallestDisplacement(10.0f)


        var locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations){
                    // Update UI with location data
                    // ...
                    if (ConnectionChecking.isConnected(this@MapsActivity)) {
                        var address = getAddress(location.latitude,location.longitude)
                        CoroutineScope(Default).launch {
                            verifyTimesVisited(address!!)
                        }
                    }
                    else {
                        for (item in listLocation!!) {
                            val dist = SphericalUtil.computeDistanceBetween(
                                LatLng(item.lat, item.lon),
                                LatLng(location.latitude, location.longitude)
                            )
                            Log.d("gps", "${item.lat} ${location.latitude} ${location.longitude}")
                            if (dist < 100 && !inSafePlace) {
                                inSafePlace = true
                                showNotification(this@MapsActivity, "You have enter a safe place")
                                showNoConnectionError(this@MapsActivity, "Someone has enter a safe place")
                            } else if (dist > 100 && inSafePlace) {
                                inSafePlace = false
                                showNotification(this@MapsActivity, "You have exit a safe place")
                                showNoConnectionError(this@MapsActivity, "Someone has left a safe place")

                            }
                        }
                    }
                }
            }
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())

        safeLocation()
    }

    private fun safeLocation() {
        val confirmbtn = binding.confirmBtnAddress

        confirmbtn.setOnClickListener {
            if (ConnectionChecking.isConnected(this)) {
                val latlongSent = LatLng(stickyLat!!, stickyLon!!)
                val addressSent = getAddress(latlongSent.latitude, latlongSent.longitude)

                val key = currentUserDb.push().key
                if(key != null ){
                    val location = SafeLocationEntity(0, key, currentUser?.uid!!, latlongSent.latitude, latlongSent.longitude, addressSent.toString())
                    currentUserDb?.push().setValue(location)
                    viewModel.insertSafeLocationInfo(location)
                }
                createGeoFence(latlongSent, currentUser.uid, geofencingClient!!)
                finish()
            }
            else
            {
                finish()
            }
        }
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.uiSettings.isZoomControlsEnabled = true

        if (!isLocationPermissionGranted()) {
            val permissions = mutableListOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            )
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                permissions.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
            }
            ActivityCompat.requestPermissions(
                    this,
                    permissions.toTypedArray(),
                    LOCATION_REQUEST_CODE
            )
        } else {

            if (ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            this.map.isMyLocationEnabled = true

            // Zoom to last known location
            fusedLocationClient.lastLocation.addOnSuccessListener {
                if (it != null) {
                    with(map) {
                        val latLng = LatLng(it.latitude, it.longitude)

                        val currentLocation = LatLng(it.latitude, it.longitude)
                        stickyLat = it.latitude
                        stickyLon = it.longitude
                        //tryToRecommendPlace(currentLocation,geofencingClient)
                        drawMarker(latLng)

                    }
                } else {
                    with(map) {
                        moveCamera(
                                CameraUpdateFactory.newLatLngZoom(
                                        LatLng(65.01355297927051, 25.464019811372978),
                                        CAMERA_ZOOM_LEVEL
                                )
                        )
                    }
                }
            }
        }

        map.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {

            override fun onMarkerDrag(p0: Marker?) {


            }

            override fun onMarkerDragEnd(p0: Marker?) {
                if (currentMarker != null) {
                    currentMarker?.remove()
                }


                val newLatLng = LatLng(p0?.position!!.latitude, p0?.position.longitude)

                drawMarker(newLatLng)
            }

            override fun onMarkerDragStart(p0: Marker?) {

            }

        })

    }

    private fun drawMarker(latlong: LatLng) {
        if (ConnectionChecking.isConnected(this)) {
            val markerOption =  MarkerOptions().position(latlong).title("Place").snippet(getAddress(latlong.latitude, latlong.longitude)).draggable(true)

            stickyLat = latlong.latitude
            stickyLon = latlong.longitude

            val textViwConfirm = binding.confirmAddress
            textViwConfirm.text = "Place" + getAddress(latlong.latitude, latlong.longitude)

            map.animateCamera(CameraUpdateFactory.newLatLng(latlong))
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlong, 15f))
            currentMarker =  map.addMarker(markerOption)
            currentMarker?.showInfoWindow()

        }
        else
        {
            val textViwConfirm = binding.confirmAddress
            binding.confirmBtnAddress.text = "Back"
            textViwConfirm.text = "You can't save safe places because you don't have connection"
            stickyLat = latlong.latitude
            stickyLon = latlong.longitude
            map.animateCamera(CameraUpdateFactory.newLatLng(latlong))
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlong, 15f))
        }
    }


    private fun getAddress(lat: Double, lon: Double) : String? {
        val geocoder =   Geocoder(this, Locale.getDefault())
        val address =   geocoder.getFromLocation(lat, lon, 1)
        return address[0].getAddressLine(0).toString()
    }

    private fun createGeoFence(location: LatLng, key: String, geofencingClient: GeofencingClient) {

        val geofence = Geofence.Builder()
            .setRequestId(GEOFENCE_ID)
            .setCircularRegion(location.latitude, location.longitude, GEOFENCE_RADIUS.toFloat())
            .setExpirationDuration(GEOFENCE_EXPIRATION.toLong())
            .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER or Geofence.GEOFENCE_TRANSITION_EXIT)
            .build()

        val geofenceRequest = GeofencingRequest.Builder()
            .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
            .addGeofence(geofence)
            .build()


        val intent = Intent(this, GeofenceReceiver::class.java)
            .putExtra("key", key)


        val pendingIntent = PendingIntent.getBroadcast(
                applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT
        )



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (ContextCompat.checkSelfPermission(
                            applicationContext, Manifest.permission.ACCESS_BACKGROUND_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                        this,
                        arrayOf(
                                Manifest.permission.ACCESS_BACKGROUND_LOCATION
                        ),
                        GEOFENCE_LOCATION_REQUEST_CODE
                )
            } else {
                geofencingClient.addGeofences(geofenceRequest, pendingIntent)

            }
        } else {
            geofencingClient.addGeofences(geofenceRequest, pendingIntent)

        }
    }

    private fun isLocationPermissionGranted() : Boolean {
        return ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == GEOFENCE_LOCATION_REQUEST_CODE) {
            if (permissions.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(
                        this,
                        "This application needs background location to work on Android 10 and higher",
                        Toast.LENGTH_SHORT
                ).show()
            }
        }
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (
                grantResults.isNotEmpty() && (
                        grantResults[0] == PackageManager.PERMISSION_GRANTED ||
                                grantResults[1] == PackageManager.PERMISSION_GRANTED)
            ) {
                if (ActivityCompat.checkSelfPermission(
                                this,
                                Manifest.permission.ACCESS_FINE_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                                this,
                                Manifest.permission.ACCESS_COARSE_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return
                }
                map.isMyLocationEnabled = true
                onMapReady(map)
            } else {
                Toast.makeText(
                        this,
                        "The app needs location permission to function",
                        Toast.LENGTH_LONG
                ).show()
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                if (grantResults.isNotEmpty() && grantResults[2] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(
                            this,
                            "This application needs background location to work on Android 10 and higher",
                            Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    companion object {

        fun showNoConnectionError(context:Context?, message: String) {
            val builder = AlertDialog.Builder(context)
            with(builder)
            {
                setTitle("No internet connection")
                setMessage("Due to no connectivity we can't send a notification to your safe contacts. ¿Do you want to send them a SMS?")

                setPositiveButton("Send SMS", DialogInterface.OnClickListener(function = { _: DialogInterface, _: Int ->
                    SendSMS.sendingSMS(context, message)
                }))

                setNegativeButton("Don't Send SMS", DialogInterface.OnClickListener(function = { _: DialogInterface, _: Int ->
                }))
                show()
            }
        }

        fun establishNotification(mess: String, toke: String) {
            val title = "Wibu"
            val body  = mess.toString()
            val user = toke.toString()
            Log.d("notification","Token: " + toke)
            val mFunctions = FirebaseFunctions.getInstance()

            val dataListA = hashMapOf(
                "title" to title,
                "body" to body,
                "user" to user
            )

            mFunctions.getHttpsCallable("sendNotification")
                .call(dataListA)
                .addOnSuccessListener {
                    Log.d("VICTORIA", "Send message to client check")
                }

        }

        fun showNotification(context: Context?, message: String) {
            val CHANNEL_ID = "REMINDER_NOTIFICATION_CHANNEL"
            var notificationId = 1589
            notificationId += Random(notificationId).nextInt(1, 30)

            val notificationBuilder = NotificationCompat.Builder(context!!.applicationContext, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_alarm)
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContentText(message)
                    .setStyle(
                            NotificationCompat.BigTextStyle()
                                    .bigText(message)
                    )
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)

            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                        CHANNEL_ID,
                        context.getString(R.string.app_name),
                        NotificationManager.IMPORTANCE_DEFAULT
                ).apply {
                    description = context.getString(R.string.app_name)
                }
                notificationManager.createNotificationChannel(channel)
            }
            notificationManager.notify(notificationId, notificationBuilder.build())
        }

    }
}