package com.example.wibuapp.fragments

import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore.Images.Media.getBitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.wibuapp.activities.AuthActivity
import com.example.wibuapp.databinding.FragmentAccountBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import java.io.ByteArrayOutputStream
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AccountFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AccountFragment : Fragment() {

    val REQUEST_CODE = 100

    val database = FirebaseDatabase.getInstance()
    val firebase_storage = FirebaseStorage.getInstance()

    private var _binding: FragmentAccountBinding? = null
    private var imageUri: Uri? = null
    private val binding get() = _binding!!

    val auth = FirebaseAuth.getInstance()
    val currentUser = auth.currentUser

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAccountBinding.inflate(inflater, container, false)
        val view = binding.root


        firebaseAnalytics = Firebase.analytics

        val preferences = this.getActivity()?.getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)
        val name = preferences?.getString("FIRSTNAME", "")
        val lastname = preferences?.getString("LASTNAME", "")
        binding.fullnameText.text = name.toString() + " " + lastname.toString()

        logout()
        return view

    }

    private fun uploadImage(){
        // Create a storage reference from our app
        if(imageUri != null){

            val bitmap =
                getBitmap(getActivity()?.getContentResolver(), imageUri)

            val storageRef = firebase_storage.reference
            val imageRef = storageRef.child("images/" + currentUser?.uid!!)

            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data = baos.toByteArray()
            var uploadTask = imageRef.putBytes(data)

            val urlTask = uploadTask.continueWithTask { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                imageRef.downloadUrl
            }.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val downloadUri = task.result
                    Toast.makeText(activity?.applicationContext, "Image uploaded successfully", Toast.LENGTH_LONG).show()

                } else {
                    // Handle failures
                }
            }
        }
    }

    private fun logout() {
        _binding?.logoutbtn?.setOnClickListener{
            val sharedPreferences = this.getActivity()?.getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)
            val editor: SharedPreferences.Editor = sharedPreferences!!.edit()
            editor.remove("FIRSTNAME")
            editor.remove("LASTNAME")
            editor.remove("TOKEN")
            editor.apply()
            auth.signOut()
            val intent = Intent(this.activity, AuthActivity::class.java)

            startActivity(intent)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    public override fun onResume() {
        super.onResume()
        recordScreenView()
    }

    private fun recordScreenView() {

        val screenName = "account-fragment"

        // [START set_current_screen]
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
            param(FirebaseAnalytics.Param.SCREEN_NAME, screenName)
            param(FirebaseAnalytics.Param.SCREEN_CLASS, "MainActivity")
        }
        // [END set_current_screen]
    }

}