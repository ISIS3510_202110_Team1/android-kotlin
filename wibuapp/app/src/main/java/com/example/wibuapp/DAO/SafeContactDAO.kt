package com.example.wibuapp.DAO

import androidx.room.*
import com.example.wibuapp.entities.SafeContactEntity

@Dao
interface SafeContactDAO {


    @Query("SELECT * FROM safecontactinfo ORDER BY id DESC")
    suspend fun getAllContactInfo(): List<SafeContactEntity>?


    @Insert
    suspend fun insertContact(contact: SafeContactEntity?)

    @Delete
    suspend fun deleteContact(contact: SafeContactEntity?)

    @Update
    suspend fun updateContact(contact: SafeContactEntity?)

}
