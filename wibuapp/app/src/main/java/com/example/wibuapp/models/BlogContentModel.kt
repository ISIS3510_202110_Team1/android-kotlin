package com.example.wibuapp.models

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.wibuapp.data_classes.BlogContent
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class BlogContentModel {
    private val auth = FirebaseAuth.getInstance()
    private val database = FirebaseDatabase.getInstance()
    private val databaseReference = database?.reference!!.child("blogentries")

    var blogContent: MutableList<BlogContent> = ArrayList()

    fun fetchblogData(): LiveData<List<BlogContent>> {
        val mutableLiveData = MutableLiveData<List<BlogContent>>()
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
                if (blogContent.size != 0) {
                    blogContent = ArrayList()
                }

                for (ds in dataSnapshot.children) {

                    val children = ds!!.children
                    children.forEach {
                        val title: String = it.child("title").getValue(String::class.java)!!
                        val author: String = it.child("author").getValue(String::class.java)!!
                        val content: String = it.child("content").getValue(String::class.java)!!
                        val date: String = it.child("date").getValue(String::class.java)!!
                        val feeling: String = it.child("feeling").getValue(String::class.java)!!
                        val token: String = it.child("token").getValue(String::class.java)!!
                        val views: Long = it.child("views").value as Long
                        val likes: Long = it.child("likes").value as Long
                        val activatedByBtn = it.child("activatedByButton").value as Boolean

                        blogContent.add(BlogContent(title, author, date, feeling, content, views, likes, token, activatedByBtn))
                    }
                }
                mutableLiveData.value = blogContent
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("firebase", "loadPost:onCancelled", databaseError.toException())
            }
        }
        databaseReference?.addValueEventListener(postListener)
        mutableLiveData.value = blogContent
        return mutableLiveData
    }
    /**
     * Function to increment the views of the given blog
     * **/
    fun increment_views(blog: BlogContent){
        databaseReference.get().addOnSuccessListener {
            for (user in it.children) {
                for (i in user.children) {
                    Log.d("blog", i.toString())
                    if (i.child("author").value as String == blog.author && i.child("title").value as String == blog.title) {
                        databaseReference.child(user.key!!).child(i.key!!).setValue(BlogContent(blog.title, blog.author, blog.date, blog.feeling,
                                blog.content, blog.views + 1, blog.likes, blog.token, blog.activatedByButton))
                        break
                    }
                }
            }
        }
    }
}
