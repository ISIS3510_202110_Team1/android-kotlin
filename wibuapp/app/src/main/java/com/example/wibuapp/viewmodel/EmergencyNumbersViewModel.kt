package com.example.wibuapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.wibuapp.DAO.EmergencyNumbersDAO
import com.example.wibuapp.models.EmergencyNumberModel
import com.example.wibuapp.entities.EmergencyNumbersEntity
import com.example.wibuapp.db.RoomAppDb
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference

class EmergencyNumbersViewModel (app: Application): AndroidViewModel(app) {

    lateinit var allNumbers : MutableLiveData<List<EmergencyNumbersEntity>>
    var refSafeNumberDao: WeakReference<EmergencyNumbersDAO?>? = null

    init{
        allNumbers = MutableLiveData()
        getAllNumbers()
    }

    val numberModel  = EmergencyNumberModel()

     fun getAllNumbersObservers(): MutableLiveData<List<EmergencyNumbersEntity>> {
        if(allNumbers.value?.size != null){
            return allNumbers
        }else {
            val mutableData =  MutableLiveData<List<EmergencyNumbersEntity>>()
            numberModel.fetchNumbersData().observeForever {
                mutableData.value = it
            }
            for( entity in mutableData.value!!){
                insertEmergencyNumberInfo(entity)
            }
            return mutableData
        }
    }

    fun updateWeakReferences(){
        if (refSafeNumberDao == null){
            val safeNumberDao = RoomAppDb.getAppDatabase((getApplication()))?.emergencyNumbersDAO()
            refSafeNumberDao = WeakReference(safeNumberDao)
        }
    }

     fun getAllNumbers() {
         updateWeakReferences()
        viewModelScope.launch(Dispatchers.IO) {
            val list = refSafeNumberDao!!.get()?.getAllEmergencyNumber()
            allNumbers.postValue(list)
        }
    }

    fun insertEmergencyNumberInfo(entity: EmergencyNumbersEntity){
        updateWeakReferences()
        viewModelScope.launch(Dispatchers.IO) {
            refSafeNumberDao!!.get()?.insertNumber(entity)
        }
        getAllNumbers()
    }
    fun increment_emergency_number_count(name:String){
        numberModel.increment_count(name)
    }
}

