package com.example.wibuapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.wibuapp.DAO.EmergencyNumbersDAO
import com.example.wibuapp.DAO.SafeContactDAO
import com.example.wibuapp.models.ContactRepository
import com.example.wibuapp.db.RoomAppDb
import com.example.wibuapp.entities.SafeContactEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference


class SafeContactViewModel(app: Application): AndroidViewModel(app) {

    lateinit var allUsers : MutableLiveData<List<SafeContactEntity>>
    var refSafeContactDao: WeakReference<SafeContactDAO?>? = null

    init{
        allUsers = MutableLiveData()
        getAllUsers()

    }

    val contactRepo  = ContactRepository()

    fun getAllUsersObservers(): MutableLiveData<List<SafeContactEntity>> {
        if(allUsers.value?.size != null){
            return allUsers
        }else {
            val mutableData =  MutableLiveData<List<SafeContactEntity>>()
            contactRepo.fetchContactData().observeForever {
                mutableData.value = it
            }
            for( entity in mutableData.value!!){
                insertSafeContactInfo(entity)
            }
            return mutableData
        }
    }

    fun updateWeakReferences(){
        if (refSafeContactDao == null){
            val safeContactDao = RoomAppDb.getAppDatabase((getApplication()))?.safeContactDAO()
            refSafeContactDao = WeakReference(safeContactDao)
        }
    }

     fun getAllUsers() {
         updateWeakReferences()
        viewModelScope.launch(Dispatchers.IO) {
            val list = refSafeContactDao!!.get()?.getAllContactInfo()
            allUsers.postValue(list)
        }
    }

     fun insertSafeContactInfo(entity: SafeContactEntity){
        updateWeakReferences()
        viewModelScope.launch(Dispatchers.IO) {
            refSafeContactDao!!.get()?.insertContact(entity)
        }
        getAllUsers()
    }

    fun updateSafeContactInfo(entity: SafeContactEntity){
        updateWeakReferences()
        viewModelScope.launch(Dispatchers.IO) {
            refSafeContactDao!!.get()?.updateContact(entity)
        }
        getAllUsers()
    }

    fun deleteSafeContactInfo(entity: SafeContactEntity){
        updateWeakReferences()
        viewModelScope.launch(Dispatchers.IO) {
            refSafeContactDao!!.get()?.deleteContact(entity)
        }
        getAllUsers()
    }
}