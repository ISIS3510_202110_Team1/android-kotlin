package com.example.wibuapp.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import com.example.wibuapp.R
import com.example.wibuapp.data_classes.BlogContent
import com.example.wibuapp.databinding.FragmentAccountBinding
import com.example.wibuapp.databinding.FragmentFormBlogBinding
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FormSafePlacesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FormSafePlacesFragment : Fragment() {

    private var _binding: FragmentFormBlogBinding? = null
    private val binding get() = _binding!!
    var feeling = "-"


    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    public override fun onResume() {
        super.onResume()
        recordScreenView()
    }

    private fun recordScreenView() {

        val screenName = "formblog-fragment"

        // [START set_current_screen]
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
            param(FirebaseAnalytics.Param.SCREEN_NAME, screenName)
            param(FirebaseAnalytics.Param.SCREEN_CLASS, "MainActivity")
        }
        // [END set_current_screen]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding = FragmentFormBlogBinding.inflate(inflater, container, false)
        val view = binding.root

        firebaseAnalytics = Firebase.analytics

        binding.CancelBlog.setOnClickListener{
            parentFragmentManager.beginTransaction().apply{
                replace(R.id.myNavHostFragment, BlogContentFragment())
                addToBackStack(null)
                commit()
            }
        }

        binding.submitBlog.setOnClickListener{

            if(binding.sadfeeling.isChecked){
                feeling = "Sad"
            }else if(binding.angryfeeling.isChecked){
                feeling = "Angry"
            }else if(binding.anxiusfeeling.isChecked){
                feeling = "Anxious"
            }else if(binding.scaredfeeling.isChecked){
                feeling = "Scared"
            }

            val authorBlog =  binding.switch1.isChecked
            val contentBlog = binding.plainTextInput.text
            val titleBlog = binding.titleEntryBlog.text
            var completeText = authorBlog.toString() + "::::" + contentBlog + "::::" + titleBlog + "::::" + feeling

            setFragmentResult("requestKeyCuatro", bundleOf("data" to completeText))

            parentFragmentManager.beginTransaction().apply{
                  replace(R.id.myNavHostFragment, BlogContentFragment())
                addToBackStack(null)
                commit()
            }

        }

        return view
    }

}