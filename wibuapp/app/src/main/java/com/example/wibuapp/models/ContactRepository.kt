package com.example.wibuapp.models

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.wibuapp.entities.SafeContactEntity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ContactRepository {

    private val auth = FirebaseAuth.getInstance()
    private val database = FirebaseDatabase.getInstance()
    private val databaseReference = database?.reference!!.child("contacts")

    var safeContacts: MutableList<SafeContactEntity> = ArrayList()

    fun fetchContactData(): LiveData<List<SafeContactEntity>> {
        val mutableLiveData = MutableLiveData<List<SafeContactEntity>>()
        val currentUser = auth.currentUser
        val currentUserDb = databaseReference?.child((currentUser?.uid!!))
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
                if (safeContacts.size != 0) {
                    safeContacts = ArrayList()
                }
                for (ds in dataSnapshot.children) {
                    val id: Int = ds.child("id").getValue(Int::class.java)!!
                    val name: String = ds.child("name").getValue(String::class.java)!!
                    val number: String = ds.child("phone").getValue(String::class.java)!!
                    val photo: String = ds.child("photoURL").getValue(String::class.java)!!
                    val key: String = ds.child("key").getValue(String::class.java)!!
                    safeContacts.add(SafeContactEntity(id, name, key, number, photo))
                }
                mutableLiveData.value = safeContacts
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("firebase", "loadPost:onCancelled", databaseError.toException())
            }
        }
        currentUserDb?.addValueEventListener(postListener)
        mutableLiveData.value = safeContacts
        return mutableLiveData
    }
}