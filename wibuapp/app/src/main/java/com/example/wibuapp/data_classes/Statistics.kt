package com.example.wibuapp.data_classes

data class Statistics(
    var name: String,
    var token: String,
    var panicButtonTimes: Long,
    var numberOfComments: Long
)