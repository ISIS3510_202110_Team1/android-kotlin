package com.example.wibuapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.wibuapp.R
import com.example.wibuapp.data_classes.BlogContent
import com.example.wibuapp.databinding.ItemBlogListBinding

class BlogAdapter (val listener: RowClickListener): RecyclerView.Adapter<BlogAdapter.MyViewHolder>() {

    var items  = ArrayList<BlogContent>()
    lateinit var view: View
    lateinit var parent: ViewGroup

    fun setListData(data: ArrayList<BlogContent>) {
        this.items = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BlogAdapter.MyViewHolder {
        this.parent = parent
        val layoutInflater = LayoutInflater.from(parent.context)
        view = layoutInflater.inflate(R.layout.item_blog_list, parent, false)

        return MyViewHolder(view, listener)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: BlogAdapter.MyViewHolder, position: Int) {

        holder.itemView.setOnClickListener {
            listener.onItemClickListener(items[position])
        }
        holder.bind(items[position],parent.context)
    }

    class MyViewHolder(view: View, val listener: RowClickListener): RecyclerView.ViewHolder(view) {


        val safeBlogEntryTitle = ItemBlogListBinding.bind(view).textView3
        val safeBlogEntryAuthor = ItemBlogListBinding.bind(view).textView4
        val safeBlogEntryDate = ItemBlogListBinding.bind(view).textView5


        fun bind(data: BlogContent, context: Context) {

            safeBlogEntryTitle.text = data.title
            safeBlogEntryAuthor.text = "By " + data.author
            safeBlogEntryDate.text = data.date

        }
    }

    interface RowClickListener{
        fun onItemClickListener(entries: BlogContent)
    }
}


