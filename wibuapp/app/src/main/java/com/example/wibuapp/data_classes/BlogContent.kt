package com.example.wibuapp.data_classes

data class BlogContent(
    var title: String,
    var author: String,
    var date: String,
    var feeling: String,
    var content: String,
    var views: Long,
    var likes : Long,
    var token: String,
    var activatedByButton: Boolean
)