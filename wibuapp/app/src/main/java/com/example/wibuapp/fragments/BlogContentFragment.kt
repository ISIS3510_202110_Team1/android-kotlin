package com.example.wibuapp.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.wibuapp.code_generic_reusable.ConnectionChecking
import com.example.wibuapp.code_generic_reusable.CustomDialog
import com.example.wibuapp.R
import com.example.wibuapp.adapters.BlogAdapter
import com.example.wibuapp.data_classes.BlogContent
import com.example.wibuapp.databinding.FragmentBlogContentBinding
import com.example.wibuapp.viewmodel.BlogViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.text.DateFormat
import java.util.*
import kotlin.collections.ArrayList
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 * Use the [BlogContentFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BlogContentFragment : Fragment() , BlogAdapter.RowClickListener {

    lateinit var viewModel: BlogViewModel
    lateinit var mRecyclerView: RecyclerView
    lateinit var recyclerViewAdapter: BlogAdapter

    private var _binding: FragmentBlogContentBinding? = null
    private val binding get() = _binding!!

    private lateinit var preferences: SharedPreferences


    var auth: FirebaseAuth = FirebaseAuth.getInstance()
    var database: FirebaseDatabase? = FirebaseDatabase.getInstance()
    val currentUser = auth.currentUser

    var databaseReference: DatabaseReference? = database?.reference!!.child("blogentries")

    val currentUserDb = databaseReference?.child((currentUser?.uid!!))

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setFragmentResultListener("requestKeyCuatro") { key, bundle ->

            val result = bundle.get("data")

            var arrayInfo = result.toString().split("::::").toTypedArray()
            var author = "Anonymous"
            val preferences = this.getActivity()?.getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE)

            var authorName = arrayInfo[0]
            if( authorName == "false"){
                val name = preferences?.getString("FIRSTNAME", "")
                val lastname = preferences?.getString("LASTNAME", "")

                author =  name.toString() + " " + lastname.toString()
            }
            var contentBlog = arrayInfo[1]
            var titleBlog = arrayInfo[2]
            var feelingBlog = arrayInfo[3]

            val calendar: Calendar = Calendar.getInstance()
            val currentDate: String = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime())

            val activatedByBtn = preferences!!.getBoolean("PANIC_BUTTON",false)
            val editor: SharedPreferences.Editor = preferences.edit()
            editor.putBoolean("PANIC_BUTTON",false)
            editor.apply()
            val token = preferences?.getString("TOKEN", "").toString()


            val key = currentUserDb?.push()?.key

            if (key != null) {
                val contentToSend = BlogContent(titleBlog, author, currentDate, feelingBlog, contentBlog,0,0, token, activatedByBtn)
                currentUserDb?.push()?.setValue(contentToSend)
            }
        }


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        _binding = FragmentBlogContentBinding.inflate(inflater, container, false)
        val view = binding.root

        firebaseAnalytics = Firebase.analytics

        if(ConnectionChecking.isConnected(view.context)){
            binding.rvBlogEntries.visibility = View.VISIBLE
            binding.noInternetView.visibility = View.GONE

        }
        else {
            binding.rvBlogEntries.visibility = View.GONE
            binding.noInternetView.visibility = View.VISIBLE
        }


        mRecyclerView = view.findViewById(R.id.rvBlogEntries) as RecyclerView

        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(view.context)

        recyclerViewAdapter = BlogAdapter(this)
        mRecyclerView.adapter = recyclerViewAdapter

        viewModel = ViewModelProviders.of(this).get(BlogViewModel::class.java)

        viewModel.getAllBlogEntriesObservers().observe( viewLifecycleOwner , Observer {
            recyclerViewAdapter.setListData(ArrayList(it))
            recyclerViewAdapter.notifyDataSetChanged()
        })


        binding.textView6.setOnClickListener{
            if(ConnectionChecking.isConnected(view.context)) {
                parentFragmentManager.beginTransaction().apply{
                    replace(R.id.myNavHostFragment, FormSafePlacesFragment())
                    addToBackStack(null)
                    commit()
                }
            } else {
                getActivity()?.supportFragmentManager?.let { it1 -> CustomDialog().show(it1, "MyCustomFragmentContact") }
            }

        }

        return view

    }


    public override fun onResume() {
        super.onResume()
        recordScreenView()
    }

    private fun recordScreenView() {

        val screenName = "blog-fragment"

        // [START set_current_screen]
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW) {
            param(FirebaseAnalytics.Param.SCREEN_NAME, screenName)
            param(FirebaseAnalytics.Param.SCREEN_CLASS, "MainActivity")
        }
        // [END set_current_screen]
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun onItemClickListener(entries: BlogContent) {

        Log.d("TUJA", "SPY VISTAS ANTES DE QUE ME MANDEN" + entries.views.toString())
        Log.d("TUJA", "SOY LIKES ANTES DE QUE ME MANDEN " + entries.likes.toString())

        val fragment = BlogDetailFragment.newInstance(entries.author, entries.date,
                entries.title, entries.content, entries.feeling, entries.token, entries.activatedByButton,
                entries.views, entries.likes)


        parentFragmentManager.beginTransaction().apply{
            replace(R.id.myNavHostFragment, fragment)
            viewModel.increment_view_blog(entries)
            addToBackStack(null)
            commit()
        }
    }


}

