package com.example.wibuapp.entities

import androidx.annotation.ColorInt
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "safecontactinfo")
data class SafeContactEntity (
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id : Int = 0 ,
        @ColumnInfo(name = "name") val name: String,
        @ColumnInfo(name = "key") val key: String,
        @ColumnInfo(name = "phone") val phone: String?,
        @ColumnInfo(name = "photoURL") val photoURL: String
)

