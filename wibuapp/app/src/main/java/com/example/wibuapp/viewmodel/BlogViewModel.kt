package com.example.wibuapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.wibuapp.data_classes.BlogContent
import com.example.wibuapp.models.BlogContentModel

class BlogViewModel(app: Application): AndroidViewModel(app)  {

    val blogModel = BlogContentModel()

    fun getAllBlogEntriesObservers(): MutableLiveData<List<BlogContent>> {
        val mutableData =  MutableLiveData<List<BlogContent>>()
        blogModel.fetchblogData().observeForever {
            mutableData.value = it
        }
        return mutableData
    }
    fun increment_view_blog(blog:BlogContent){
        blogModel.increment_views(blog)
    }
}



