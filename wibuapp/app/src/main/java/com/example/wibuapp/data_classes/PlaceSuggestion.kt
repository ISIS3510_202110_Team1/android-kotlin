package com.example.wibuapp.data_classes

data class PlaceSuggestion(
        var name: String,
        var timesVisited: Int,
        var accepted: Boolean,
        var user: String
)
