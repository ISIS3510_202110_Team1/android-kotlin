package com.example.wibuapp.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.wibuapp.DAO.EmergencyNumbersDAO
import com.example.wibuapp.DAO.SafeContactDAO
import com.example.wibuapp.DAO.SafeLocationDAO
import com.example.wibuapp.entities.EmergencyNumbersEntity
import com.example.wibuapp.entities.SafeContactEntity
import com.example.wibuapp.entities.SafeLocationEntity

@Database(entities = arrayOf(
        SafeContactEntity::class,
        SafeLocationEntity::class,
        EmergencyNumbersEntity::class),
        version = 6)
abstract class RoomAppDb: RoomDatabase() {

    abstract fun safeContactDAO(): SafeContactDAO?
    abstract fun emergencyNumbersDAO(): EmergencyNumbersDAO?
    abstract fun safeLocationDAO(): SafeLocationDAO?

    companion object {
        private var INSTANCE: RoomAppDb?= null

        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE IF NOT EXISTS `emergencynumber` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT NOT NULL, `phone` TEXT NOT NULL  )")
            }
        }

        fun getAppDatabase(context: Context): RoomAppDb? {

            if(INSTANCE == null ) {

                INSTANCE = Room.databaseBuilder<RoomAppDb>(
                        context.applicationContext, RoomAppDb::class.java, "AppDBB"
                )

                        .allowMainThreadQueries()
                        .addMigrations(MIGRATION_1_2)
                        .build()

            }
            return INSTANCE
        }
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}