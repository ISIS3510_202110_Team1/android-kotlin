package com.example.wibuapp.data_classes


data class Reminder(
        var key: String = "",
        var user: String = "",
        var lat: Double = 0.0,
        var lon: Double = 0.0,
        var address: String = ""
)