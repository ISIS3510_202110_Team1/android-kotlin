package com.example.wibuapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.wibuapp.R
import com.example.wibuapp.databinding.ItemSafeplaceListBinding
import com.example.wibuapp.entities.SafeLocationEntity


class SafePlacesAdapter(val listener: RowClickListenerPlaces): RecyclerView.Adapter<SafePlacesAdapter.MyViewHolder>() {

    var items  = ArrayList<SafeLocationEntity>()
    lateinit var view:View
    lateinit var parent:ViewGroup

    fun setListData(data: ArrayList<SafeLocationEntity>) {
        this.items = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SafePlacesAdapter.MyViewHolder {
        this.parent = parent
        val layoutInflater = LayoutInflater.from(parent.context)
        view = layoutInflater.inflate(R.layout.item_safeplace_list, parent, false)

        return SafePlacesAdapter.MyViewHolder(view, listener)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: SafePlacesAdapter.MyViewHolder, position: Int) {

        holder.itemView.setOnClickListener {
            listener.onItemClickListener(items[position])
        }
        holder.bind(items[position])
    }

    class MyViewHolder(view: View, val listener: RowClickListenerPlaces): RecyclerView.ViewHolder(view) {

        val safePlaceName = ItemSafeplaceListBinding.bind(view).tvSafePlaceName
        var deleteButton = ItemSafeplaceListBinding.bind(view).imgDelete
        var editButton = ItemSafeplaceListBinding.bind(view).imgEdit


        fun bind(data: SafeLocationEntity) {
            safePlaceName.text = data.address
            deleteButton.setOnClickListener {
                listener.onDeleteLocationClickListener(data, it.context)
            }
        }
    }

    interface RowClickListenerPlaces{
        fun onDeleteLocationClickListener(location: SafeLocationEntity, context: Context)
        fun onItemClickListener(location: SafeLocationEntity)
    }

}
